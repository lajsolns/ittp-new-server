const {Router} = require('express');

require('../db'); // to db
require('../utils/passport-config'); // to load passport configuration

module.exports = (app, apiVersion) => {
    const router = Router();
    app.use(apiVersion, router);

    router.use('/auth', require('./auth.router'));
    router.use('/user', require('./user.router'));
    router.use('/select-option', require('./select-option.router'));
    router.use('/theme', require('./theme.router'));
    router.use('/store', require('./store.router'));
    router.use('/subscription', require('./subscription.router'));
    router.use('/my-themes', require('./my-themes.router'));
    router.use('/theme-manager', require('./my-themes.router'));
    router.use('/admin', require('./admin.router'));
    router.use('/test', require('./test.router'));
    router.use('/mobile-api', require('./mobile-api.router'));
    router.use('/media', require('./media.router'));
};
