const {Router} = require('express');
const asyncHandler = require('express-async-handler');

const core = require('../core');
const passportConfig = require('../utils/passport-config');

const {
    selectOptionService,
    routeParams,
    deviceService,
    userService,
    statsService,
} = core.services;


const router = Router();


router.use(passportConfig.required, routeParams.userInfoRouteGuard, (req, res, next) => {
    if (req.user.isAdmin) {
        next();
    } else {
        res.status(403).end({msg: 'User not admin'});
    }
});


router.get('/', (req, res, next) => {
    res.send('Hello Admin');
});


router.get('/itt-user', asyncHandler(async (req, res, next) => {
    const page = parseInt(req.query.page, 10) || 1;
    const limit = parseInt(req.query.pageSize, 10) || 10;
    const users = await userService.getAllUsersPaginate({page, limit});
    res.send(users)
}));


router.get('/itt-user/:userId/detail', routeParams.userIdParam, asyncHandler(async (req, res, next) => {
    const {userId, user} = req.context;
    const {email, isAdmin, name, profileImageUrl} = user;
    const userDetails = {id: userId, email, name, isAdmin, profileImageUrl};
    const devices = await deviceService.findUserDevices(userId);
    const subscriptions = await userService.getUserSubscribedThemes(userId);
    const themes = await userService.getUserCreatedThemes(userId);
    userDetails.devices = devices;
    userDetails.subscriptions = subscriptions;
    userDetails.themes = themes;
    res.send(userDetails);
}));


router.get('/admin-user', asyncHandler(async (req, res, next) => {
    const page = parseInt(req.query.page, 10) || 1;
    const limit = parseInt(req.query.pageSize, 10) || 10;
    const adminUsers = await userService.getAdminUsersPaginate({page, limit});
    res.send(adminUsers)
}));

router.post('/admin-user/add', asyncHandler(async (req, res, next) => {
    const {userId} = req.body;
    await userService.addAddAdminUser(userId);
    res.send({msg: 'User added as admin'});
}));


router.post('/admin-user/remove', asyncHandler(async (req, res, next) => {
    const {userId} = req.body;
    await userService.removeAdminUser(userId);
    res.send({msg: 'User removed as admin'});
}));


router.post('/device/:deviceId/location', routeParams.deviceIdParam, asyncHandler(async (req, res, next) => {
    const page = parseInt(req.query.page, 10) || 1;
    const limit = parseInt(req.query.pageSize, 10) || 10;
    const {start, end} = req.body;
    const startTime = parseFloat(start);
    const endTime = parseFloat(end);
    const deviceId = req.params.deviceId;
    const deviceLocations = await deviceService.getDeviceLocationsPaginate({page, limit, deviceId, startTime, endTime});
    res.send(deviceLocations);
}));


// router.post('/device/:deviceId/location/download', routeParams.deviceIdParam, (req, res, next) => {
//     const {start, end} = req.body;
//     const startTime = parseFloat(start);
//     const endTime = parseFloat(end);
//     const params = {startTime, endTime};
//     params.res = res;
//     params.next = next;
//     const deviceId = req.params.deviceId;
//     deviceService.downloadDeviceLocationsKmlFile(params);
// });


router.get('/select-option/category/:categoryId', asyncHandler(async (req, res, next) => {
    const category = await selectOptionService.getCategoryById(req.params.categoryId);
    res.send(category);
}));


router.post('/select-option/category/remove', asyncHandler(async (req, res, next) => {
    const id = req.body.categoryId;
    await selectOptionService.deleteOneCategory(id);
    res.send({msg: 'category deleted'})
}));

router.post('/select-option/category/:categoryId/edit', asyncHandler(async (req, res, next) => {
    await selectOptionService.editCategory(req.body);
    res.send({msg: 'Category Saved'})
}));


router.post('/select-option/category/create', asyncHandler(async (req, res, next) => {
    await selectOptionService.createCategory(req.body);
    res.send({msg: 'Category Created'})
}));

router.post('/select-option/region/remove', asyncHandler(async (req, res, next) => {
    const id = req.body.regionId;
    await selectOptionService.deleteOneRegion(id);
    res.send({msg: 'Region deleted'});
}));


router.post('/select-option/region/create', asyncHandler(async (req, res, next) => {
    await selectOptionService.createRegion(req.body);
    res.send({msg: 'Region Created'});
}));


router.get('/select-option/region/:regionId', asyncHandler(async (req, res, next) => {
    const region = await selectOptionService.getRegionById(req.params.regionId);
    res.send(region);
}));


router.post('/select-option/region/:regionId', asyncHandler(async (req, res, next) => {
    req.body.id = req.params.regionId;
    await selectOptionService.editRegion(req.body);
    res.send({msg: 'Region Saved'});
}));


router.post('/update-stats', (req, res, next) => {
    statsService.updateAllStats();
    res.status(201).send('Updating Stats for all themes');
});


module.exports = router;
