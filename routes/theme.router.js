const {Router} = require('express');
const asyncHandler = require('express-async-handler');


const core = require('../core');
const {routeParams, paymentService, themeService, tagFileProcessing, mediaService, deviceService} = core.services;

const router = Router();

router.param('themeId', routeParams.themeIdParam);


router.get('/:themeId/file-download/template', (req, res, next) => {
    const theme = req.context.theme;
    const templateFile = tagFileProcessing
        .operations
        .generateTagTemplateFile(theme);

    res.writeHead(200, {
        'Content-Type': 'application/octet-stream',
        'Content-Disposition': `attachment;filename=${templateFile.filename}`
    });
    res.write(templateFile.content);
    res.end();
});


router.get('/:themeId/file-download/tags', (req, res, next) => {
    const theme = req.context.theme;
    tagFileProcessing
        .operations
        .generateExistingTagsFile({res, next, theme});
});


router.get('/:themeId/header', routeParams.themeIdParam, (req, res, next) => {
    const themeId = req.params.themeId;
    res.send({
        id: themeId,
        name: req.context.theme.basicInfo.name,
        themeImageUrl: mediaService.getMediaUrls(themeId).themeIcon
    });
});

router.get('/stats/:themeId/subscriptions', asyncHandler(async (req, res, next) => {
    const themeId = req.context.themeId;
    const ratingStats = await themeService.getRatingStats(themeId);
    res.send(ratingStats);
}));


router.get('/theme/:themeId/tags/new-tag', (req, res, next) => {
});


router.post('/theme/:themeId/tags/create', (req, res, next) => {
});


router.post('/theme/:themeId/tags/save', (req, res, next) => {
});


router.get('/theme/:themeId/tags/:tagId', (req, res, next) => {
});

router.get('/device/:deviceId/location/download/:start/:end', routeParams.deviceIdParam, (req, res, next) => {
    const {start, end} = req.params;
    const startTime = parseFloat(start);
    const endTime = parseFloat(end);
    const params = {startTime, endTime};
    params.deviceId = req.params.deviceId;
    params.res = res;
    params.next = next;
    deviceService.downloadDeviceLocationsKmlFile(params);
});


module.exports = router;
