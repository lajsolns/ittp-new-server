const {Router} = require('express');
const passport = require('passport');
const jwt = require('jsonwebtoken');

const router = Router();

function generateTokenResponse(user) {
    const {_id: id ,profileImageUrl, name, isAdmin} = user;
    const _id = id.toString();
    const token = jwt.sign({_id, name}, process.env.SECRET);
    console.log('token:', token);
    return {token, isAdmin, name, profileImageUrl, _id};
}

function handleAuthResult(err, user, info, res, next) {
    if (err) {
        err.errMessage = info ? info.message : 'Login failed';
        return next(err);
    }

    if (!user) {
        return res.status(401).json({
            message: info ? info.message : 'Login failed',
        });
    }

    return res.send(generateTokenResponse(user));
}

router.post('/', (req, res, next) => {
    passport.authenticate('firebase', {session: false}, (err, user, info) => {
        return handleAuthResult(err, user, info, res, next);
    })(req, res, next);
});


module.exports = router;