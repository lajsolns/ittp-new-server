const {Router} = require('express');
const asyncHandler = require('express-async-handler');
const core = require('../core');

const {types} = core.lib;
const {selectOptionService} = core.services;

const router = Router();


router.get('/', (req, res, next) => {
    const type = req.query.q;

    switch (type) {
        case 'region':
            getRegions(req, res, next);
            break;
        case 'category':
            getCategories(req, res, next);
            break;
        default:
            if (req.query.q) {
                getOtherTypes(req, res, next)
            } else {
                getAllTypes(req, res, next);
            }
            break;
    }
});


const getCategories = asyncHandler(async (req, res, next) => {
    const categories = await selectOptionService.getCategories();
    const values = categories.map(category => {
        const {label, order, value, description, id} = category;
        return {label, order, value, description, id};
    });
    res.send(values);

});


const getRegions = asyncHandler(async (req, res, next) => {

    const regions = selectOptionService.getRegions();
    const values = regions.map(region => {
        const {value, order, label, description, id} = region;
        return {value, order, label, description, id};
    });
    res.send(values);
});


const getAllTypes = asyncHandler(async (req, res, next) => {
    const regionsPms = await selectOptionService.getRegions();
    const categoriesPms = await selectOptionService.getCategories();
    types.regions = regionsPms;
    types.categories = categoriesPms;
    res.send(types);
});


const getOtherTypes = asyncHandler(async (req, res, next) => {
    const arr = types[req.query.q];
    if (arr) {
        res.send(arr);
    } else {
        res.status(404).send('No Type found');
    }
});


module.exports = router;