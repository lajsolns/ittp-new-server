const {Router} = require('express');
const asyncHandler = require('express-async-handler');

const core = require('../core');
const passportConfig = require('../utils/passport-config');
const fileUploader = require('../utils/file-upload');

const {userService, authService, mediaService, userProfileImageService} = core.services;

const router = Router();

router.get('/me', passportConfig.required, asyncHandler(async (req, res, next) => {
    const user = await userService.findUserById(req.user._id);
    const {_id, name, profileImageUrl, email} = user;
    res.send({_id, name, profileImageUrl, email});
}));

router.post('/profile-image-upload', passportConfig.required, fileUploader, asyncHandler(async (req, res, next) => {
    const userId = req.user._id;
    const filePath = req.file.path;
    const result = await userProfileImageService.saveUserProfileImage({userId, filePath});
    res.send({msg: result});
}));

router.post('/search-user', asyncHandler(async (req, res, next) => {
    const searchTerm = req.body.searchInput;
    const user = await userService.searchUser(searchTerm);
    res.send(user);
}));

router.post('/search-adminuser', asyncHandler(async (req, res, next) => {
    const searchTerm = req.body.searchInput;
    const user = await userService.searchAdminUser(searchTerm);
    res.send(user);
}));

router.get('/:id/search-exact-user', asyncHandler(async (req, res, next) => {
    const id = req.params.id;
    const user = await userService.getUser(id);
    res.send(user);
}));

module.exports = router;
