const {Router} = require('express');
const asyncHandler = require('express-async-handler');

const core = require('../core');
const passportConfig = require('../utils/passport-config');

const {subscriptionService, paymentService, routeParams} = core.services;

const router = Router();


router.param('themeId', routeParams.themeIdParam);

router.get('/', (req, res, next) => {
    res.send('Hello from subscriptions');
});

// router.get('/user-themes', asyncHandler(async (req, res, next) => {
//     const userThemes = await subscriptionService.getUserSubscriptions(req.query.userId);
//     const themeIds = userThemes.map(userTheme => userTheme.themeId);
//     res.send(themeIds);
// }));

router.post('/theme/unsubscribe/:themeId', passportConfig.optional, asyncHandler(async (req, res, next) => {
    const themeId = req.context.themeId;
    const userId = req.body.userId || req.user._id;
    const status = await subscriptionService.getUserThemeSubscriptionStatus({userId, themeId});
    if (!status.isSubscribed) {
        // no susbscription
        return res.send({message: 'no previous subscription'});
    }
    await subscriptionService.unsubscribeUserFromTheme({userId, themeId});
    res.send({message: 'user unsubscribed'});
}));


module.exports = router;
