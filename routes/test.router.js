const {Router} = require('express');
const asyncHandler = require('express-async-handler');

const core = require('../core');
const passportConfig = require('../utils/passport-config');

const {
    storeService,
    selectOptionService,
    paymentService,
    subscriptionService,
    routeParams,
    themeService,
    wishlistService,
    tagService,
    deviceService,
    azureService,
    statsService
} = core.services;


const router = Router();


router.get('/', (req, res, next) => {
    const theme = {id: "255d3d5d", name: "popcorn", description: "for cinema night" };
    res.send(theme);
});


router.get('/wishlist', passportConfig.required, (req, res, next) => {
    const userId = req.user._id;
    wishlistService.getWishListTheme(userId)
        .then(themes => {
            console.log(themes);
        });
});

router.post('/locations', (req, res, next) => {
    const params = req.body;
    deviceService
        .getDeviceLocationsPaginate(params)
        .then(locs => res.send(locs))
        .catch(err => next(err));
});


router.get('/stats', (req, res, next) => {
    themeService.find({})
        .then(themes => {
            const arr = themes.map(theme => {
                return statsService.updateThemeStats(theme._id.toString());
            });
            Promise.all(arr).then(result => {
                res.send({msg: 'Stats updated'});
            })
        })
});


router.get('/store', (req, res, next) => {
    // res.send('hello world!');
    const page = 1, limit = 10, category = 0, isPaid = true;
    storeService
        .getHomePageCategory({page, limit, category, isPaid})
        .then(result => res.send(result))
        .catch(next);
});

router.get('/subscription',  (req, res, next) => {
res.send({stats: 'my stats'});
});

router.get('/select-option/category/categories', (req, res, next) => {
    selectOptionService.getCategories()
        .then((result) => {
            res.send(result);
        });
    // res.send({m:'message'});
});


router.post('/send-msg', (req, res, next) => {
    const {tag} = req.query;
    const data = {code: 1, content: 'hello world'};
    azureService
        .sendMessageToAndroidDevice({tag, data})
        .then(result => {
            res.send('message sent');
        })
        .catch(next);
});


router.get('/notify', asyncHandler(async (req, res, next) => {
    const result = await azureService.notifySubscriptionChange('5c7c14bde149a007175352d6');
    console.log(result);
    res.send('success');
}));


router.post('/update-stats', (req, res, next) => {
    statsService.updateAllStats(next);
    res.status(201).send('Updating Stats for all themes');
});

module.exports = router;
