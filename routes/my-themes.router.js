const {Router} = require('express');
const asyncHandler = require('express-async-handler');

const core = require('../core');
const logger = require('../utils/logger');
const fileUploader = require('../utils/file-upload');

const passportConfig = require('../utils/passport-config');

const {
    mediaService,
    paymentService,
    subscriptionService,
    routeParams,
    themeService,
    tagService,
    userService,
    tagFileProcessing,
    themeStatsService,
    saveService,
    accesslistService,
    azureService,
    inviteService

} = core.services;

const router = Router();

router.use(passportConfig.required);
router.use(routeParams.userInfoRouteGuard);

// loads only user themes or admin
router.param('themeId', (req, res, next, themeId) => {
    if (req.user.isAdmin) {
        themeService
            .findThemeById(themeId)
            .then(theme => {
                if (theme) {
                    req.context.theme = theme;
                    req.context.themeId = themeId;
                    next();
                } else {
                    res.status(404).send({message: 'theme not found'});
                }
            })
            .catch(next);
    } else {
        const userId = req.user._id;
        themeService
            .findUserTheme({themeId, userId})
            .then(theme => {
                if (theme) {
                    req.context.theme = theme;
                    req.context.themeId = themeId;
                    next();
                } else {
                    res.status(404).send({message: 'theme not found'});
                }
            })
            .catch(next);
    }
});

router.param('tagId', routeParams.tagIdParam);


router.get('/new-theme', (req, res, next) => {
    res.send(themeService.newBasicInfo());
});


router.get('/theme/user-themes', asyncHandler(async (req, res, next) => {
    const userId = req.user._id;
    const page = parseInt(req.query.page, 10) || 1;
    const limit = parseInt(req.query.pageSize, 10) || 10;
    let qry = {};

    if (!req.user.isAdmin) {
        qry['_storage.createdBy'] = userId;
    }

    const themes = await themeService.getThemesPaginate({qry, page, limit});
    res.send(themes)
}));


router.get('/theme/:themeId', (req, res, next) => {
    res.send(req.context.theme);
});


router.get('/theme/:themeId/basic-info', (req, res, next) => {
    res.send(req.context.theme.basicInfo);
});


router.get('/theme/:themeId/tag-properties', (req, res, next) => {
    res.send(req.context.theme.tagProperties);
});


router.get('/theme/:themeId/settings', (req, res, next) => {
    res.send(req.context.theme.settings);
});

router.post('/theme/create', asyncHandler(async (req, res, next) => {
    const basicInfo = req.body;
    const user = req.user;
    const savedTheme = await themeService.createTheme({basicInfo, user});
    res.status(201).send(savedTheme._id);
}));


router.delete('/theme/delete/:themeId', asyncHandler(async (req, res, next) => {
    const themeId = req.params.themeId;
    await themeService.deleteTheme(themeId);
    res.send({msg: 'theme deleted'});
}));


router.post('/theme/:themeId/basic-info', (req, res, next) => {
    const theme = req.context.theme;
    const user = req.user;
    theme.basicInfo = req.body;
    saveThemeAndReturn({theme, user, res, next});
});


router.get('/theme/:themeId/subscription/:userId', asyncHandler(async (req, res, next) => {
    const {themeId, userId} = req.params;
    const userSubscriptionDetail = await subscriptionService.getUserSubscriptionDetail({themeId, userId});
    res.send(userSubscriptionDetail);
}));


router.post('/theme/:themeId/tag-permissions/:userId', asyncHandler(async (req, res, next) => {
    const {themeId, userId} = req.params;
    const tagPermissions = req.body;
    await subscriptionService.updateTagPermissions({themeId, userId, tagPermissions});
    res.send({msg: 'Tag Permissions Saved'});
}));


router.post('/theme/:themeId/tag-properties', (req, res, next) => {
    const theme = req.context.theme;
    const user = req.user;
    theme.tagProperties = req.body;
    saveThemeAndReturn({theme, user, res, next});
});


router.post('/theme/:themeId/settings', asyncHandler(async (req, res, next) => {
    const theme = req.context.theme;
    const user = req.user;
    const newSettings = req.body;

    const params = {};
    params.themeId = req.params.themeId;
    params.themeName = req.context.themeName;
    params.oldPrice = theme.settings.price;
    params.newPrice = newSettings.price;
    params.newCurrency = newSettings.currency;
    params.oldCurrency = theme.settings.currency;

    if (themeService.hasPriceChanged(params)) {
        await paymentService.updateThemeBillingPlan(params);
        theme.settings = newSettings;
        return saveThemeAndReturn({theme, res, next, user});
    } else {
        theme.settings = newSettings;
        return saveThemeAndReturn({theme, res, next, user});
    }
}));


const saveThemeAndReturn = async ({theme, res, next, user}) => {
    theme.saveNotificationDistance();
    theme.saveSilenceTime();
    azureService.notifyThemeChange(theme._id.toString());
    await saveService.update({save: theme, user});
    res.send({msg: 'Theme saved'})
};


router.get('/theme/:themeId/detail', asyncHandler(async (req, res, next) => {
    const themeManagerSummary = await themeService.getThemeManagerSummary(req.context.theme, next);
    res.send(themeManagerSummary);
}));


router.get('/theme/:themeId/subscribers', asyncHandler(async (req, res, next) => {
    const page = parseInt(req.query.page, 10) || 1;
    const limit = parseInt(req.query.pageSize, 10) || 10;
    const themeId = req.context.themeId;
    const themeUsers = await subscriptionService.getThemeUsers({themeId, page, limit});
    res.send(themeUsers)

}));


router.get('/theme/:themeId/section-header', (req, res, next) => {
    const summary = themeService.getSummary(req.context.theme);
    const sectionHeaderData = {};
    sectionHeaderData._id = summary._id;
    sectionHeaderData.name = summary.name;
    sectionHeaderData.themeImgUrl = summary.themeImageUrl;
    res.send(sectionHeaderData);
});


router.get('/theme/:themeId/tags', async (req, res, next) => {
    const page = parseInt(req.query.page, 10) || 1;
    const limit = parseInt(req.query.pageSize, 10) || 10;
    const themeId = req.context.themeId;
    const tags = await themeService.getTagsPaginate({page, limit, themeId});
    res.send(tags);
});


router.post('/theme/:themeId/tags/map', asyncHandler(async (req, res, next) => {
    const themeId = req.context.themeId;
    const {bottomLeft, upperRight} = req.body;
    const projection = {location: 1};
    const tags = await tagService.getTagsInBounds({themeId, bottomLeft, upperRight, projection});
    const markers = tags.map(x => {
        const [longitude, latitude] = x.location.coordinates;
        return {id: x._id, latitude, longitude}
    });
    const iconUrl = mediaService.getMediaUrls(themeId).marker24;
    res.send({markers, iconUrl});
}));


router.get('/theme/:themeId/tags/new-tag', (req, res, next) => {
    const lat = req.query.latitude || 0.0;
    const lon = req.query.longitude || 0.0;
    const theme = req.context.theme;
    res.send(themeService.newTag({theme, lat, lon}));
});


router.post('/theme/:themeId/tags/create', asyncHandler(async (req, res, next) => {
    const user = req.user;
    const tag = req.body;
    const themeId = req.context.themeId;
    await tagService.createTag({tag, user, themeId});
    res.send({msg: 'Tag Created'});
    const count = await tagService.getTagsCountByTheme(themeId);
    await themeStatsService.updateTagCount(themeId, count);
}));


router.post('/theme/:themeId/tags/:tagId/save', asyncHandler(async (req, res, next) => {
    const existing = req.context.tag;
    const toSave = req.body;
    const user = req.user;
    await tagService.updateTag({existing, toSave, user});
    res.send({msg: 'Tag Saved'});
}));


router.get('/theme/:themeId/tag-edit-model/tag/:tagId', routeParams.tagIdParam, (req, res, next) => {
    const {tagProperties} = req.context.theme;
    const tag = req.context.tag;
    res.send({tagProperties, tag});
});


router.get('/theme/:themeId/tag-edit-model/new', (req, res, next) => {
    const lat = req.query.latitude || 0.0;
    const lon = req.query.longitude || 0.0;
    const theme = req.context.theme;
    const {tagProperties} = req.context.theme;
    const tag = themeService.newTag({theme, lat, lon});
    res.send({tagProperties, tag, isNew: true});
});


router.get('/theme/:themeId/tags/:tagId', (req, res, next) => {
    res.send(req.context.tag);
});


router.post('/theme/:themeId/file-upload/new', fileUploader, asyncHandler(async (req, res, next) => {
    const {themeId} = req.params;
    const tagProperties = req.context.theme.tagProperties;
    const userId = req.user.name;
    const {path, size, originalname: name} = req.file;

    const tagOperation = await tagFileProcessing.operations.insertNewTagsOperation({
        themeId,
        userId,
        name,
        size,
        path
    });
    tagFileProcessing.processors.new({tagOperation, tagProperties});
    res.send({msg: 'File queue for processing'});
}));


router.post('/theme/:themeId/file-upload/updated', fileUploader, asyncHandler(async (req, res, next) => {
    const {themeId} = req.params;
    const tagProperties = req.context.theme.tagProperties;
    const userId = req.user.name;
    const {path, size, originalname: name} = req.file;

    const tagOperation = await tagFileProcessing.operations.insertUpdatedTagsOperation({
        themeId,
        userId,
        name,
        size,
        path
    });
    tagFileProcessing.processors.update({tagOperation, tagProperties});
    res.send({msg: 'File queue for processing'});
}));


router.get('/theme/:themeId/file-operations', asyncHandler(async (req, res, next) => {
    const fileOperations = await tagFileProcessing.operations.getFileOperationsForTheme(req.context.themeId);
    res.send(fileOperations)
}));


router.get('/users', asyncHandler(async (req, res, next) => {
    const page = parseInt(req.query.page, 10) || 1;
    const limit = parseInt(req.query.pageSize, 10) || 10;
    const users = await userService.getAllUsersPaginate({page, limit});
    res.send(users);
}));


router.post('/users/theme-invite', (req, res, next) => {
    res.send(inviteService.sendMail(req.body.themeId, req.body.userId))
});


router.post('/theme/:themeId/media-upload', fileUploader, asyncHandler(async (req, res, next) => {
    const themeId = req.context.themeId;
    const filePath = req.file.path;
    await mediaService.saveThemeMedia({themeId, filePath});
    res.send({msg: 'Image Saved'})
}));


router.get('/theme/:themeId/wishlist', asyncHandler(async (req, res, next) => {
    const page = parseInt(req.query.page, 10) || 1;
    const limit = parseInt(req.query.pageSize, 10) || 10;
    const themeId = req.params.themeId;
    const wishlist = await themeService.getWishlistPaginated({page, limit, themeId});
    res.send(wishlist);
}));


// Subscribe From Wishlist
router.post('/subscription/subscribe/:themeId/:userId', asyncHandler(async (req, res, next) => {
    const {userId, themeId} = req.params;
    const status = await subscriptionService.getUserThemeSubscriptionStatus({userId, themeId});
    if (status.isSubscribed) {
        res.send({msg: 'Already Subscribed'});
    } else {
        const isSubscribed = await subscriptionService.subscribeUserToThemeWishlist({userId, themeId});
        res.send({msg: 'Subscribed', id: isSubscribed._id});
    }

}));


router.post('/search-theme', asyncHandler(async (req, res, next) => {
    const theme = await themeService.search(req.body.searchInput);
    res.send(theme);
}));


router.get('/:themeId/search-subscriber/:searchInput', asyncHandler(async (req, res, next) => {
    const {searchInput, themeId} = req.params;
    const themeUsers = await subscriptionService.searchThemeUsers({searchInput, themeId});
    res.send({count: themeUsers.length, data: themeUsers, page: 1});
}));


router.get('/access-list/:themeId/white-list', routeParams.themeIdParam, asyncHandler(async (req, res, next) => {
    const page = parseInt(req.query.page, 10) || 1;
    const limit = parseInt(req.query.pageSize, 10) || 10;
    const themeId = req.context.themeId;
    const whitelist = await themeService.getWhiteListPaginated({page, limit, themeId});
    console.log(whitelist);
    res.send(whitelist);
}));

router.post('/:userId/:themeId/white-list/remove', routeParams.themeIdParam, asyncHandler(async (req, res, next) => {
    const themeId = req.context.themeId;
    const userId = req.params.userId;
    await accesslistService.removeUserFromList({themeId, userId});
    res.send({msg: 'removed'});
}));


router.post('/:userId/:themeId/white-list', asyncHandler(async (req, res, next) => {
    const userId = req.params.userId;
    const themeId = req.params.themeId;
    await accesslistService.addUserToList({themeId, userId});
    res.send({msg: 'Added to white-list'});
}));


router.get('/stats/:themeId/subscriptions', asyncHandler(async (req, res, next) => {
    const themeId = req.context.themeId;
    const subscriptionStats = await themeService.getSubscriptionsStats(themeId);
    res.send(subscriptionStats);
}));

router.get('/stats/:themeId/ratings', asyncHandler(async (req, res, next) => {
    const themeId = req.context.themeId;
    const ratingStats = await themeService.getRatingStats(themeId);
    res.send(ratingStats);
}));

router.get('/stats/:themeId/tags', asyncHandler(async (req, res, next) => {
    const themeId = req.context.themeId;
    const tagStats = await themeService.getTagsStats(themeId);
    res.send(tagStats);
}));


router.post('/access-list/:themeId/emails', fileUploader, (req, res, next) => {
    //ToDO
});


router.post('/access-list/:themeId/ids', fileUploader, (req, res, next) => {
    //ToDO
});


module.exports = router;
