const {Router} = require('express');
const asyncHandler = require('express-async-handler');

const core = require('../core');
const passportConfig = require('../utils/passport-config');

const {
    storeService,
    selectOptionService,
    paymentService,
    subscriptionService,
    routeParams,
    themeService,
    wishlistService,
    deviceService,
    themeRatingService,
    statsService
} = core.services;


const router = Router();


router.param('themeId', routeParams.themeIdParam);

router.get('/home-page-models', asyncHandler(async (req, res, next) => {
    const topFree = await storeService.getTopFree();
    const topPaid = await storeService.getTopPaid();
    const categories = await selectOptionService.getCategories();
    res.send({
        topFree,
        topPaid,
        categories
    });
}));

router.get('/top-free', asyncHandler(async (req, res, next) => {
    const obj = {};
    obj.page = parseInt(req.query.currentPage, 10) || 1;
    obj.limit = parseInt(req.query.pageSize, 10) || 10;
    const topFreePaginated = await storeService.getTopFreePaginated(obj);
    res.send(topFreePaginated);
}));

router.get('/top-paid', asyncHandler(async (req, res, next) => {
    const obj = {};
    obj.page = parseInt(req.query.currentPage, 10) || 1;
    obj.limit = parseInt(req.query.pageSize, 10) || 10;
    const topPaidPaginated = await storeService.getTopPaidPaginated(obj);
    res.send(topPaidPaginated);
}));

router.get('/category/:categoryId/free', asyncHandler(async (req, res, next) => {
    const obj = {};
    obj.page = parseInt(req.query.currentPage, 10) || 1;
    obj.limit = parseInt(req.query.pageSize, 10) || 10;
    obj.categoryId = parseInt(req.params.categoryId, 10);
    const themes = await storeService.getFreeThemePerCategory(obj);
    res.send(themes);
}));

router.get('/category/:categoryId/paid', asyncHandler(async (req, res, next) => {
    const obj = {};
    obj.page = parseInt(req.query.currentPage, 10) || 1;
    obj.limit = parseInt(req.query.pageSize, 10) || 10;
    obj.categoryId = req.params.categoryId;
    const paidTheme = await storeService.getPaidThemePerCategory(obj);
    res.send(paidTheme);
}));

router.get('/similar-themes/:themeId', asyncHandler(async (req, res, next) => {
    const themeId = req.params.themeId;
    const similarThemes = await storeService.getSimilarThemes(themeId);
    res.send(similarThemes);
}));

router.get('/theme-detail/:themeId', (req, res, next) => {
    res.send(themeService.getThemeStoreSummary(req.context.theme));
});

router.get('/theme-search', asyncHandler(async (req, res, next) => {
    const themes = await storeService.search(req.query.q);
    res.send(themes);
}));


router.get('/subscription-info/:themeId', passportConfig.required, asyncHandler(async (req, res, next) => {
    const userId = req.user._id;
    const themeId = req.context.themeId;
    const subscriptionInfo = await subscriptionService.getUserThemeSubscriptionInfo(userId, themeId);
    res.send(subscriptionInfo);
}));

router.get('/subscription-status/:themeId', passportConfig.required, asyncHandler(async (req, res, next) => {
    const userId = req.user._id;
    const themeId = req.params.themeId;
    const status = await subscriptionService.getUserThemeSubscriptionStatus({themeId, userId});
    res.send(status);
}));

router.post('/subscription/create-billing-agreement/:themeId', passportConfig.required, asyncHandler(async (req, res, next) => {
    const themeName = req.context.theme.basicInfo.name;
    const themeDescription = req.context.theme.basicInfo.description;
    const userId = req.user._id;
    const params = {themeName, themeDescription, userId};
    const billingPlan = await paymentService.getLatestThemeBillingPlan(req.context.themeId);
    params.billingPlanId = billingPlan.billingPlanId;
    const agreement = await paymentService.createBillingAgreement(params);
    res.status(201).send(agreement);
}));


router.post('/subscription/execute-billing-agreement/:themeId', passportConfig.required, asyncHandler(async (req, res, next) => {
    const userId = req.user._id;
    const {token} = req.body;
    const themeId = req.params.themeId;
    const payment = await paymentService.executePayment({userId, token, themeId});
    res.status(201).send({msg: 'Payment created', id: payment._id});
}));

router.post('/subscription/subscribe/:themeId', passportConfig.required, asyncHandler(async (req, res, next) => {
    const userId = req.user._id;
    const themeId = req.context.themeId;
    const status = await subscriptionService.getUserThemeSubscriptionStatus({userId, themeId});
    if (status.isSubscribed) {
        return res.send({message: 'User Already Subscribed'});
    }
    const result = await subscriptionService.subscribeUserToThemeNormal({userId, themeId});
    if (result.paymentRequired) {
        return res.status(400).send({msg: 'Payment required'});
    } else {
        res.status(201).send({msg: 'User Subscribed'})
    }
}));


router.get('/subscription/user-themes', passportConfig.required, asyncHandler(async (req, res, next) => {
    const themes = await subscriptionService.getUserSubscriptions(req.user._id);
    res.send(themes);
}));

router.post('/wishlist/add/:themeId', passportConfig.required, asyncHandler(async (req, res, next) => {
    const themeId = req.context.themeId;
    const userId = req.user._id;
    await wishlistService.addThemeToUserWishlist({themeId, userId});
    res.send({msg: 'Theme added to Wishlist'});
}));

router.post('/wishlist/delete/:themeId', passportConfig.required, asyncHandler(async (req, res, next) => {
    const themeId = req.context.themeId;
    const userId = req.user._id;
    await wishlistService.removeThemeFromUserWishlist({themeId, userId});
    res.send({msg: 'Theme removed from Wishlist'});
}));

router.get('/wishlist/check/:themeId', passportConfig.required, asyncHandler(async (req, res, next) => {
    const themeId = req.context.themeId;
    const userId = req.user._id;
    const isOnWishList = await wishlistService.isThemeOnUserWishlist({themeId, userId});
    res.send({isOnWishlist: isOnWishList});
}));

router.get('/user-wishlists', passportConfig.required, asyncHandler(async (req, res, next) => {
    obj = {};
    obj.userId = req.user._id;
    obj.page = parseInt(req.query.currentPage, 10) || 1;
    obj.limit = parseInt(req.query.pageSize, 10) || 10;
    const themes = await wishlistService.getWishListTheme(obj);
    res.send(themes);
}));

router.get('/user-payments', passportConfig.required, asyncHandler(async (req, res, next) => {
    obj = {};
    obj.userId = req.user._id;
    obj.page = parseInt(req.query.currentPage, 10) || 1;
    obj.limit = parseInt(req.query.pageSize, 10) || 10;
    const payments = await paymentService.getUserPaymentsPaginated(obj);
    res.send(payments);
}));


router.get('/user-devices', passportConfig.required, asyncHandler(async (req, res, next) => {
    const page = parseInt(req.query.page, 10) || 1;
    const limit = parseInt(req.query.pageSize, 10) || 10;
    const userId = req.user._id;
    const userDevices = await deviceService.findUserDevicesPaginate({userId, page, limit});
    res.send(userDevices);
}));


router.get('/user-subscriptions', passportConfig.required, asyncHandler(async (req, res, next) => {
    const page = parseInt(req.query.page, 10) || 1;
    const limit = parseInt(req.query.pageSize, 10) || 10;
    const userId = req.user._id;
    const subscriptions = await subscriptionService.getUserSubscriptionsPaginate({userId, page, limit});
    res.send(subscriptions);
}));


router.post('/review/add', asyncHandler(async (req, res, next) => {
    await themeRatingService.createReview(req.body);
    res.send({msg: 'review added'});
}));

router.post('/review/edit', asyncHandler(async (req, res, next) => {
    const params = req.body;
    await themeRatingService.updateReview(params);
    res.send({msg: 'review updated'});
}));


router.get('/review/ratings/:themeId', routeParams.themeIdParam, (req, res, next) => {
    const result = {labels: [1, 2, 3, 4, 5], data: []};
    const reviewStats = req.context.theme.stats.ratings;
    result.data.push(reviewStats.data[0]);
    result.data.push(reviewStats.data[1]);
    result.data.push(reviewStats.data[2]);
    result.data.push(reviewStats.data[3]);
    result.data.push(reviewStats.data[4]);
    res.send(result);
});


router.get('/review/:themeId', asyncHandler(async (req, res, next) => {
    const page = req.query.currentPage;
    const limit = Number.parseInt(req.query.pageSize, 10);
    const themeId = req.params.themeId;
    const reviews = await themeRatingService.getAllReviewsPaginate({page, limit, themeId});
    res.send(reviews);
}));


router.get('/my-review/:themeId', asyncHandler(async (req, res, next) => {
    const themeId = req.params.themeId;
    const userId = req.query.userId;
    const reviews = await themeRatingService.getReviewByUserId(themeId, userId);
    res.send(reviews);
}));


module.exports = router;
