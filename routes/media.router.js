const {Router} = require('express');

const core = require('../core');
const passportConfig = require('../utils/passport-config');
const fileUploader = require('../utils/file-upload');

const {
    mediaService,
    userProfileImageService
} = core.services;


const router = Router();


router.get('/', (req, res, next) => {
    res.send('Hello test');
});


router.get('/theme/:themeId/:type', (req, res, next) => {
    const {themeId, type} = req.params;
    const isFileTypeAllowed = mediaService.allowedTypes.indexOf(type) !== -1;
    if (isFileTypeAllowed) {
        mediaService
            .findMediaByThemeIdAndType({themeId, type, res, next});
    } else {
        res.status(404).send({msg: 'Unknown File Type'});
    }
});


router.get('/user/:userId/profile-image', (req, res, next) => {
    const {userId} = req.params;
    userProfileImageService
        .getUserProfileImage({userId, res, next});
});


router.get('/user-profile-image', passportConfig.required, (req, res, next) => {
    const {userId} = req.user._id;
    userProfileImageService
        .getUserProfileImage({userId, res, next});
});


module.exports = router;