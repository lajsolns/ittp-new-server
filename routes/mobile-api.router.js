const { Router } = require('express');
const asyncHandler = require('express-async-handler');

const core = require('../core');
const passportConfig = require('../utils/passport-config');

const {
    subscriptionService,
    deviceService,
    mobileApiService
} = core.services;


const router = Router();

router.use(passportConfig.required);


router.post('/device/register', asyncHandler(async (req, res, next) => {
    const userId = req.user._id;
    const params = req.body;
    params.userId = userId;
    const token = await deviceService.registerUserDevice(params);
    res.send({ token });
}));


router.get('/user-subscriptions', asyncHandler(async (req, res, next) => {
    const subscriptionIds = await subscriptionService.getUserSubscriptions(req.user._id);
    res.send(subscriptionIds);
}));


router.post('/download-themes', asyncHandler(async (req, res, next) => {
    const userId = req.user._id;
    const themeIdList = req.body;
    const themeModels = await mobileApiService.getThemes({ userId, themeIdList });
    res.send(themeModels);
}));


router.post('/download-tags', asyncHandler(async (req, res, next) => {
    const tags = await mobileApiService.getTags(req.body);
    res.send(tags);
}));


router.post('/download-tags-near-location', asyncHandler(async (req, res, next) => {
    const tagVersions = await mobileApiService.getTagsNearLocation(req.body);
    res.send(tagVersions);
}));


router.post('/download-tags-in-box', asyncHandler(async (req, res, next) => {
    const tagVersions = await mobileApiService.getThemeTagsInBox(req.body);
    res.send(tagVersions);
}));


router.post('/upload-device-location', mobileApiService.deviceTokenHeaderParam, asyncHandler(async (req, res, next) => {
    // const {locations} = req.body;
    const locations = req.body;
    locations.forEach(deviceLoc => {
        deviceLoc.location = {coordinates: [deviceLoc.longitude, deviceLoc.latitude], type: 'Point'};
        deviceLoc.deviceId = req.context.deviceId;
    });
    console.log(await mobileApiService.saveDeviceLocation(locations));
    res.send({ msg: 'Location Saved' });
}));


module.exports = router;
