const express = require('express');
const morgan = require('morgan');
const helmet = require('helmet');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const errorhandler = require('errorhandler');
const dotenv = require('dotenv');
const blueBird = require("bluebird");
const cors = require("cors");


global.Promise = blueBird; // set Promise object
mongoose.Promise = blueBird;
dotenv.config(); // load .env variables

const routes = require('./routes');

const logger = require('./utils/logger');

const app = express();

app.use(cors());
app.use(helmet());
app.use(morgan('combined', {stream: logger.stream}));
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

//create context object
app.use((req, res, next) => {
    req.context = {};
    next()
});

// configure routes
routes(app, '/api/v1/');

// test route
app.get('/', (req, res) => {
    logger.debug('get home');
    res.send('Welcome home');
});


// set up error handlers
if (process.env.NODE_ENV === 'development') {
    app.use(errorhandler());
} else {
    app.use((err, req, res, next) => {
        logger.error(err);
        const statusCode = err.statusCode || 500;
        const message = err.errMessage || 'Something went wrong';
        res.status(statusCode).send({message});
    });
}

function startServer() {
    mongoose
        .connect(process.env.DB_URI, {useNewUrlParser: true})
        .then(db => {
            // logger.debug('db connected');
            console.log('db connected');
            app.listen(process.env.PORT, () => {
                // logger.debug(`app running on port ${process.env.PORT}`);
                console.log(`app running on port ${process.env.PORT}`);
            });
        })
        .catch(err => logger.error('error connecting to db', err));
}

startServer();
