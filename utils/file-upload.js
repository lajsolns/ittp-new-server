const multer  = require('multer');
const upload = multer({ dest: 'uploads/' });
// const upload = multer();

module.exports = upload.single('file');
