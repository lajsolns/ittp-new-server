const passport = require('passport');
const expressJwt = require('express-jwt');
const CustomStrategy = require('passport-custom').Strategy;
const firebaseAdmin = require('firebase-admin');

const serviceAccountKey = require('./firebase-service-account-key');
const mongoose = require('mongoose');

const User = mongoose.model('User');

firebaseAdmin.initializeApp({
    credential: firebaseAdmin.credential.cert(serviceAccountKey)
});

const defaultPictureUrl = 'http://s3.amazonaws.com/nvest/Blank_Club_Website_Avatar_Gray.jpg';

function upsertFirebaseUser({uid: firebaseId, email, displayName: name, photoURL}) {
    const profileImageUrl = photoURL || defaultPictureUrl;
    return new Promise((resolve, reject) => {
        User
            .findOne({email})
            .then(user => {
                if (user) {
                    return resolve(user);
                } else {
                    new User({email, profileImageUrl, name, firebaseId, isAdmin: false})
                        .save()
                        .then(resolve)
                        .catch(reject);
                }
            })
            .catch(reject);
    });
}


passport.use('firebase', new CustomStrategy((req, done) => {
    const {token} = req.body;
    firebaseAdmin
        .auth()
        .verifyIdToken(token)
        .then(decodedToken => firebaseAdmin.auth().getUser(decodedToken.uid))
        .then(user => {
            const {uid, providerData} = user;
            const {email, photoURL, displayName} = providerData[0];
            return upsertFirebaseUser({uid, email, photoURL, displayName});
        })
        .then(user => done(null, user))
        .catch(done);
}));


module.exports = {
    required: expressJwt({
        secret: process.env.SECRET,
    }),
    optional: expressJwt({
        secret: process.env.SECRET,
        credentialsRequired: false
    })
};

