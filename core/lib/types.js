// exports.time = [
//     {value: 0, label: 'Minutes', order: 1},
//     {value: 1, label: 'Hours', order: 2},
//     {value: 2, label: 'Days', order: 3},
//     {value: 3, label: 'Weeks', order: 4},
//     {value: 4, label: 'Months', order: 5}
// ];
//
// exports.distance = [
//     {value: 0, label: 'Meters', order: 1},
//     {value: 1, label: 'Km', order: 2},
//     {value: 2, label: 'Feet', order: 3},
//     {value: 3, label: 'Yards', order: 4},
//     {value: 4, label: 'Miles', order: 5}
// ];
//
// exports.tagPropertyType = [
//     {value: 0, label: 'Text', order: 1},
//     {value: 1, label: 'Phone Number', order: 2},
//     {value: 2, label: 'Email Address', order: 3},
//     {value: 3, label: 'Web Address', order: 4},
//     {value: 4, label: 'Image Link', order: 5},
//     {value: 5, label: 'Video Link', order: 6},
//     {value: 6, label: 'Numbers Only', order: 7},
//     {value: 7, label: 'Address', order: 8}
// ];
//
// exports.currency = [
//     {value: 'USD', label: 'US $', order: 1}
// ];


exports.time = [
    {value: 0, label: 'Minutes', order: 1},
    {value: 1, label: 'Hours', order: 2},
    {value: 2, label: 'Days', order: 3},
    {value: 3, label: 'Weeks', order: 4},
    {value: 4, label: 'Months', order: 5}
];

exports.distance = [
    {value: 0, label: 'Meters', order: 1},
    {value: 1, label: 'Km', order: 2},
    {value: 2, label: 'Feet', order: 3},
    {value: 3, label: 'Yards', order: 4},
    {value: 4, label: 'Miles', order: 5}
];

exports.tagPropertyType = [
    {value: 0, label: 'Text', order: 1},
    {value: 1, label: 'Phone Number', order: 2},
    {value: 2, label: 'Email Address', order: 3},
    {value: 3, label: 'Web Address', order: 4},
    {value: 4, label: 'Image Link', order: 5},
    {value: 5, label: 'Video Link', order: 6},
    {value: 6, label: 'Numbers Only', order: 7},
    {value: 7, label: 'Address', order: 8}
];

exports.currency = [
    {value: 'USD', label: 'US $', order: 1}
];