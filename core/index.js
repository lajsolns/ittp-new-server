const {promisify} = require("es6-promisify");
const fs = require('fs');

exports.lib = require('./lib');

exports.services = require('./services');

exports.unlinkAsync = promisify(fs.unlink);
