exports.userService = require('./user.service');

exports.azureService = require('./azure-notification.service');

exports.selectOptionService = require('./selection-option.service');

exports.storeService = require('./store.service');

exports.subscriptionService = require('./subscription.service');

exports.themeService = require('./theme.service');

exports.routeParams = require('./route-params');

exports.paymentService = require('./payment.service');

exports.wishlistService = require('./wishlist.service');

exports.tagService = require('./tag.service');

exports.deviceService = require('./device.service');

exports.mediaService = require('./media.service');

exports.themeRatingService = require('./theme-rating.service');

exports.userProfileImageService = require('./user-profile-image.service');

exports.tagFileProcessing = require('./tag-file-processing');

exports.statsService = require('./stats.service');

exports.saveService = require('./save.service');

exports.accesslistService = require('./accesslist.service');

exports.accesslistService = require('./accesslist.service');

exports.mobileApiService = require('./mobile-api.service');

exports.inviteService = require('./invite.service');

