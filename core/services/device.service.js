const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const asyncHandler = require('express-async-handler');

const Device = mongoose.model('Device');
const DeviceLocation = mongoose.model('DeviceLocation');

const subsSvc = require('./subscription.service');
const azureSvc = require('./azure-notification.service');


exports.findUserDevices = (userId) => {
    const projection = {
        hardwareId: 1,
        manufacturer: 1,
        model: 1,
        operatingSystem: 1,
        operatingSystemVersion: 1,
        installDate: 1
    };

    return Device.find({userId}, projection, {sort: {updatedAt: -1}});
};


exports.getDeviceLocationsPaginate = async ({startTime, endTime, deviceId, page, limit}) => {
    const myCustomLabels = {
        totalDocs: 'count',
        docs: 'data',
    };

    const options = {
        page,
        limit,
        select: {speed: 1, location: 1, acquired: 1, isOffline: 1},
        customLabels: myCustomLabels,
        sort: {acquired: -1}
    };

    const qry = {deviceId, acquired: {$gte: startTime, $lte: endTime}};

    const deviceLocation = await DeviceLocation.paginate(qry, options);
    deviceLocation.data = deviceLocation.data.map(loc => {
        const [longitude, latitude] = loc.location.coordinates;
        const {_id: id, speed, acquired, isOffline} = loc;
        return {id, speed, acquired, longitude, latitude, isOnline: !isOffline};
    });
    return deviceLocation;
};


exports.downloadDeviceLocationsKmlFile = asyncHandler(async ({startTime, endTime, deviceId, res, next}) => {
    const qry = {deviceId: deviceId, acquired: {$gte: startTime, $lte: endTime}};
    const cursor = await DeviceLocation.find(qry).sort({createdAt: -1}).cursor();
    await writeKmlDocumentTypeToFile(res);
    await cursor.eachAsync(location => {
        addPlacemark(res, location);
        res.write('\n');
    });
    await closeKmlRootDocument(res);
    res.end()

});

function writeKmlDocumentTypeToFile(res, next) {
    try{
        return res.write(`<?xml version="1.0" encoding="utf-8"?>\n<kml xmlns="http://www.opengis.net/kml/2.2">\n<Document>`);
    } catch (e) {
        next(e)
    }
}

 function closeKmlRootDocument(res, next) {
    try{
        return res.write('</Document>\n</kml>');
    } catch (e) {
        next(e)
    }
}

function addPlacemark(res, location, next) {
    try {
        return res.write(`<Placemark>\n<TimeStamp>\n<when>${location.updatedAt}</when>\n</TimeStamp>\n<Point>\n<coordinates>${location.location.coordinates[0]},${location.location.coordinates[1]}</coordinates>\n</Point>\n</Placemark>`);
    } catch (e) {
        next(e)
    }
}

exports.findDeviceById = (deviceId) => {
    return Device.findById(deviceId);
};


exports.findUserDevicesPaginate = async ({userId, page, limit}) => {
    const myCustomLabels = {
        totalDocs: 'count',
        docs: 'data',
    };

    const options = {
        page,
        limit,
        customLabels: myCustomLabels,
        sort: {acquired: -1}
    };

    const qry = {userId};

    const devices = await Device.paginate(qry, options);

    devices.data = [
        {
            _id: '5bd88a13503dda16fb3a6017',
            hardwareId: 'Genymotion Custom Android',
            manufacturer: 'Genymotion',
            model: 'Custom',
            operatingSystem: 'Android',
            operatingSystemVersion: '8.0.0',
            installDate: 'Oct 31, 2018'
        },
        {
            _id: '5bd88a13503dda16fb3a6017',
            hardwareId: 'Genymotion Custom Android',
            manufacturer: 'Genymotion',
            model: 'Custom',
            operatingSystem: 'Android',
            operatingSystemVersion: '8.0.0',
            installDate: 'Oct 31, 2018'
        }
    ];

    return devices;

};


const getUserTags = async (userId) => {
    const themeIds = await subsSvc.getUserSubscriptions(userId);
    themeIds.push('itagtip', userId);
    return themeIds;
};


exports.registerUserDevice = async (params) => {
    const {userId, model, hardwareId, token} = params;
    const tags = await getUserTags(userId);
    params.azureRegistrationId = await azureSvc.registerAndroidDevice({token, tags});
    const device = await Device.findOneAndUpdate({userId, model, hardwareId}, params, {upsert: true, new: true});
    const id = device._id.toString();
    return jwt.sign({id, userId}, process.env.SECRET);
};


exports.refreshUserAzureTags = async (userId) => {
    // get all user devices update tags
    const tags = await getUserTags(userId);
    const devices = await Device.find({userId});

    return devices.map(device => {
        const {token, azureRegistrationId: regId} = device;
        return azureSvc.updateDeviceTags({token, regId, tags});
    });

};
