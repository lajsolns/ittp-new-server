const mongoose = require('mongoose');

const core = require('../../core');
const mediaService = require('./media.service');
const saveService = require('./save.service');

const Theme = mongoose.model('Theme');
const Tag = mongoose.model('Tag');
const Wishlist = mongoose.model('Wishlist');
const AccessList = mongoose.model('AccessList');
const User = mongoose.model('User');
const Category = mongoose.model('Category');
const Region = mongoose.model('Region');
const themePreviewProjection = {basicInfo: 1, stats: 1};


const {types} = core.lib;


exports.findThemeById = (id) => {
    return Theme.findById(id);
};


exports.findTheme = (qry) => {
    return Theme.findOne(qry);
};

exports.find = (qry) => {
    return Theme.find(qry);
};


exports.findUserTheme = ({userId, themeId}) => {
    const _id = mongoose.Types.ObjectId(themeId);
    const qry = {_id, "_storage.createdBy": userId};
    return Theme.findOne(qry);
};


exports.getThemesPaginate = async ({qry, page, limit}) => {
    const myCustomLabels = {
        totalDocs: 'count',
        docs: 'data',
    };

    const options = {
        page, limit,
        select: {basicInfo: 1, stats: 1},
        customLabels: myCustomLabels,
        sort: {updatedAt: -1}
    };

    const themes = await Theme.paginate(qry, options);
    themes.data = themes.data.map(theme => {
        const _id = theme._id.toString();
        const obj = {_id};
        obj.name = theme.basicInfo.name;
        obj.description = theme.basicInfo.description;
        obj.tagCount = theme.stats.tagCount;
        obj.themeImageUrl = mediaService.getMediaUrls(_id).themeIcon;
        return obj;
    });
    return themes;
};


exports.newBasicInfo = () => {
    return new Theme().basicInfo;
};


exports.createTheme = ({user, basicInfo}) => {
    const newTheme = new Theme({basicInfo});
    newTheme.saveNotificationDistance();
    newTheme.saveSilenceTime();
    return saveService.create({save: newTheme, user});
};


exports.deleteTheme = (themeId) => {
    return Theme.deleteOne({_id: themeId});
};


exports.updateTheme = (params) = {};

exports.getThemeStoreSummary = function (theme) {
    const _id = theme._id.toString();
    const summary = {_id};
    summary.name = theme.basicInfo.name;
    summary.description = theme.basicInfo.description;
    summary.createdAt = theme.createdAt;
    summary.updatedAt = theme.updatedAt;
    summary.createdBy = theme._storage.createdBy;
    summary.updatedBy = theme._storage.updatedBy;
    summary.color = theme.basicInfo.color;
    summary.visibleInStore = theme.settings.visibleInStore;
    summary.useAccessList = theme.settings.useAccessList;

    const priceStr = theme.settings.price || '0';
    const price = parseInt(priceStr, 10);
    if (price) {
        const currencyUnit = types.currency.find(x => x.value === theme.settings.currency);
        summary.price = `${currencyUnit.label} ${price}`;
    } else {
        summary.price = 'Free';
    }

    const distanceUnit = types.distance.find(x => x.value === theme.basicInfo.defaultDistanceUnit);
    summary.defaultNotificationDistance = `${theme.basicInfo.defaultDistance} ${distanceUnit.label}`;

    const silenceUnit = types.time.find(x => x.value === theme.basicInfo.defaultTagSilenceUnit);
    summary.defaultTagSilence = `${theme.basicInfo.defaultTagSilence} ${silenceUnit.label}`;

    summary.activeSubscribers = theme.stats.subscriptions.total.subscribe;
    summary.totalNumberOfRatings = theme.stats.ratings.count;
    summary.averageRating = theme.stats.ratings.average;
    summary.tagCount = theme.stats.tags.count;

    const iconUrls = mediaService.getMediaUrls(_id);
    summary.themeImageUrl = iconUrls.themeIcon;
    summary.tagIconImageUrl = iconUrls.marker32;
    return summary;
};
exports.getThemeManagerSummary = async (theme, next) => {
    const _id = theme._id.toString();
    const summary = {_id};
    summary.name = theme.basicInfo.name;
    summary.description = theme.basicInfo.description;
    summary.createdAt = theme.createdAt;
    summary.updatedAt = theme.updatedAt;
    summary.createdBy = theme._storage.createdBy;
    summary.updatedBy = theme._storage.updatedBy;
    summary.color = theme.basicInfo.color;
    summary.visibleInStore = theme.settings.visibleInStore;
    summary.allowFastTags = theme.settings.allowFastTags;
    summary.allowMovingTags = theme.settings.allowMovingTags;
    summary.allowCrowdSourcing = theme.settings.allowCrowdSourcing;
    summary.region = theme.settings.region;
    summary.category = theme.settings.category;
    summary.useAccessList = theme.settings.useAccessList;

    const priceStr = theme.settings.price || '0';
    const price = parseInt(priceStr, 10);
    if (price) {
        const currencyUnit = types.currency.find(x => x.value === theme.settings.currency);
        summary.price = `${currencyUnit.label} ${price}`;
    } else {
        summary.price = 'Free';
    }


    const distanceUnit = types.distance.find(x => x.value === theme.basicInfo.defaultDistanceUnit);
    summary.defaultNotificationDistance = `${theme.basicInfo.defaultDistance} ${distanceUnit.label}`;

    const silenceUnit = types.time.find(x => x.value === theme.basicInfo.defaultTagSilenceUnit);
    summary.defaultTagSilence = `${theme.basicInfo.defaultTagSilence} ${silenceUnit.label}`;

    summary.activeSubscribers = theme.stats.subscriptions.total.subscribe;
    summary.totalNumberOfRatings = theme.stats.ratings.count;
    summary.averageRating = theme.stats.ratings.average;
    summary.tagCount = theme.stats.tags.count;

    const iconUrls = mediaService.getMediaUrls(_id);
    summary.themeImageUrl = iconUrls.themeIcon;
    summary.tagIconImageUrl = iconUrls.marker32;

    //Get category label and Region Label
    const category = await Category.find({value: {$in: summary.category}});
    const region = await Region.find({value: {$in: summary.region}});
    summary.category = category.length > 0 ? getCategoriesLabel(category) : '';
    summary.region = region.length > 0 ? getRegionsLabel(region) : '';
    console.log(summary);
    return summary;

};

function getCategoriesLabel(categories) {
    return categories.map(cat => cat.label)
}

function getRegionsLabel(regions) {
    return regions.map(cat => cat.label)
}

exports.getTagsPaginate = async ({page, limit, themeId}) => {
    const myCustomLabels = {
        totalDocs: 'count',
        docs: 'data',
    };

    const options = {
        page, limit,
        select: {location: 1, _storage: 1, updatedAt: 1},
        customLabels: myCustomLabels,
        sort: {updatedAt: -1}
    };

    const qry = {themeId};

    const tags = await Tag.paginate(qry, options);
    tags.data = tags.data.map(tag => {
        const obj = {};
        obj._id = tag._id;
        obj.latitude = tag.location.coordinates[1];
        obj.longitude = tag.location.coordinates[0];
        obj.updatedAt = tag.updatedAt;
        obj.updatedBy = tag._storage.updatedBy;
        return obj;
    });
    return tags;
};


exports.newTag = ({lat, lon, theme}) => {
    return theme.newTag(lat, lon);
};


exports.findThemesInIdArray = ({list, projection}) => {
    const idList = list.map(id => mongoose.Types.ObjectId(id));
    const qry = {_id: {$in: idList}};
    return Theme.find(qry, projection);
};


exports.getUserCreatedThemes = async (userId) => {
    const themes = await Theme.find({'_storage.createdBy': userId}, {basicInfo: 1});
    return themes.map(theme => {
        const id = theme._id.toString();
        const projection = {id};
        projection.imageUrl = mediaService.getMediaUrls(id).themeIcon;
        projection.name = theme.basicInfo.name;
        return projection;
    });
};

exports.getWishlistPaginated = async ({page, limit, themeId}) => {
    const myCustomLabels = {
        totalDocs: 'count',
        docs: 'data',
    };

    const options = {
        page, limit,
        select: {_id: 1, name: 1},
        customLabels: myCustomLabels,
    };

    const wishlists = await Wishlist.find({themeId: themeId})
    const userIds = wishlists.map(e => e.userId);
    const qry = {_id: {$in: userIds}};
    return User.paginate(qry, options);
};

exports.searchSubscribers = (searchTerm) => {

};

exports.search = async (searchTerm) => {
    const themes = await Theme.find({
        "basicInfo.name": {
            "$regex": searchTerm,
            "$options": "i"
        },
        '_storage.deleted': false,
        'storeProperties.visibleInStore': true
    }, themePreviewProjection);
    return themes.map(theme => getThemeListModel(theme));
};

function getThemeListModel(theme) {
    const _id = theme._id.toString();
    const obj = {_id};
    obj.name = theme.basicInfo.name;
    obj.description = theme.basicInfo.description;
    obj.tagCount = theme.stats.tagCount;
    obj.themeImageUrl = mediaService.getMediaUrls(_id).themeIcon;
    return obj;
}

exports.getWhiteListPaginated = async ({page, limit, themeId}) => {
    const myCustomLabels = {
        totalDocs: 'count',
        docs: 'data',
    };

    const options = {
        page, limit,
        select: {_id: 1, email: 1},
        customLabels: myCustomLabels,
    };


    const users = await AccessList.find({themeId: themeId});
    const userIds = users.map(e => e.userId);
    const qry = {_id: {$in: userIds}};
    return User.paginate(qry, options);
};

exports.getRatingStats = (themeId) => {
    return Theme.findOne({_id: themeId}, {"stats.ratings": 1});
};

exports.getSubscriptionsStats = (themeId) => {
    return Theme.findOne({_id: themeId}, {"stats.subscriptions": 1});
};

exports.getTagsStats = (themeId) => {
    return Theme.findOne({_id: themeId}, {"stats.tags": 1});
};

exports.addToWhiteList = ({id, themeId}) => {
    const accessList = new AccessList();
    accessList.themeId = themeId;
    accessList.userId = id;
    accessList.accessType = 2;

    return accessList.save();
};

exports.hasPriceChanged = ({oldPrice, newPrice, newCurrency, oldCurrency}) => {
    return (oldPrice !== newPrice) || (oldCurrency !== newCurrency);
};

exports.convertUnits = (basicInfo) => {

};
