const mongoose = require('mongoose');
const moment = require('moment');
const Theme = mongoose.model('Theme');
const Tag = mongoose.model('Tag');
const Subscription = mongoose.model('Subscription');
const Review = mongoose.model('Review');
const Wishlist = mongoose.model('Wishlist');

function logCompletion(theme) {
    console.log(`Theme: ${theme.basicInfo.name} Stats Updated`);
}

function logError(err) {
    console.log(`Error Updating Stat`, err);
}

const getTagCount = (themeId) => {
    return Tag.countDocuments({themeId});
};


const getRatingsCountAndAverage = (themeId) => {
    const $match = {themeId};
    const $project = {rating: 1};
    const $group = {
        _id: '$themeId',
        count: {$sum: 1},
        average: {$avg: '$rating'}
    };
    return Review
        .aggregate([
            {$match},
            {$project},
            {$group}
        ])
        .then(results => {
            return (results && results[0]) || {average: 0, count: 0};
        });
};


const subscriptionsTimeRange = (params) => {
    const {themeId, start} = params;
    const $match = {themeId, createdAt: {$gte: start}};
    const $project = {active: 1};
    const $group = {_id: '$active', num: {$sum: 1}};

    return Subscription
        .aggregate([
            {$match},
            {$project},
            {$group}
        ])
        .then(results => {
            const stat = {};
            if (results.length) {
                const subscribeCount = results.find(x => x._id === true);
                if (subscribeCount) {
                    stat.subscribe = subscribeCount.num;
                } else {
                    stat.subscribe = 0;
                }

                const unsubscribeCount = results.find(x => x._id === false);
                if (unsubscribeCount) {
                    stat.unsubscribe = unsubscribeCount.num;
                } else {
                    stat.unsubscribe = 0;
                }
            } else {
                stat.subscribe = 0;
                stat.unsubscribe = 0;
            }

            return stat;
        });
};


const getSubscriptionsTimeRange = (themeId) => {
    const last7daysStart = moment().subtract(7, 'days').startOf('day').toDate();
    const last30daysStart = moment().subtract(30, 'days').startOf('day').toDate();
    const todayStart = moment().startOf('day').toDate();
    const overall = new Date(0);

    return Promise
        .all([
            subscriptionsTimeRange({themeId, start: todayStart}),
            subscriptionsTimeRange({themeId, start: last7daysStart}),
            subscriptionsTimeRange({themeId, start: last30daysStart}),
            subscriptionsTimeRange({themeId, start: overall})
        ])
        .then(results => {
            const subscribe = [];
            const unsubscribe = [];
            results.forEach(x => {
                subscribe.push(x.subscribe);
                unsubscribe.push(x.unsubscribe);
            });

            const overallIdx = 3;
            const total = {subscribe: subscribe[overallIdx], unsubscribe: unsubscribe[overallIdx]};
            const updatedAt = new Date();
            return {data: {unsubscribe, subscribe}, total, updatedAt};
        });
};


const getRatingsSummary = (themeId) => {
    const $match = {themeId};
    const $project = {rating: 1};
    const $group = {_id: '$rating', num: {$sum: 1}};

    return Review
        .aggregate([
            {$match},
            {$project},
            {$group}
        ])
        .then(results => {
            const summary = [];

            function getRatingFromResult(number) {
                if (results.length) {
                    const rating = results.find(x => x._id === number);
                    if (rating) {
                        summary.push(rating.num);
                    } else {
                        summary.push(0);
                    }
                } else {
                    summary.push(0);
                }
            }

            getRatingFromResult(1);
            getRatingFromResult(2);
            getRatingFromResult(3);
            getRatingFromResult(4);
            getRatingFromResult(5);

            return summary;
        });
};


const updateTagStats = async (themeId) => {
    const count = await getTagCount(themeId);
    const tags = {count: count, updatedAt: new Date()};
    try {
        const theme = await Theme.findByIdAndUpdate(themeId, {'stats.tags': tags}).exec();
        logCompletion(theme)
    } catch (err) {
        logError(err);
    }
};


const updateRatingStats = async (themeId) => {
    const summary = await getRatingsSummary(themeId);
    const avgCount = await getRatingsCountAndAverage(themeId);
    const stats = {updatedAt: new Date()};
    stats.average = avgCount.average;
    stats.count = avgCount.count;
    stats.data = summary;
    try {
        const theme = await Theme.findByIdAndUpdate(themeId, {'stats.ratings': stats}).exec();
        logCompletion(theme)
    } catch (err) {
        logError(err);
    }
};

const updateSubscriptionStats = async (themeId) => {
    const subscriptions = await getSubscriptionsTimeRange(themeId);
    try {
        const theme = await Theme.findByIdAndUpdate(themeId, {'stats.subscriptions': subscriptions}).exec();
        logCompletion(theme)
    }catch(err) {
        logError(err);
    }
};


const updateThemeStats = (themeId) => {
    updateSubscriptionStats(themeId);
    updateWishlistCount(themeId);
    updateTagStats(themeId);
    updateRatingStats(themeId);
};


const getWishListCount = (themeId) => {
    return Wishlist.countDocuments({themeId});
};

const updateWishlistCount = async (themeId) => {

    const count = await getWishListCount(themeId);
    const wishList = {count, updatedAt: new Date()};
    try {
        const theme = await Theme.findByIdAndUpdate(themeId, {'stats.wishList': wishList}).exec();
        logCompletion(theme);
    } catch(err) {
        logError(err);
    }
};


const updateAllStats = async () => {
    const themes = await Theme.find({});
    themes.forEach(x => updateThemeStats(x._id.toString()))
};

module.exports = {
    updateSubscriptionStats,
    updateWishlistCount,
    updateTagStats,
    updateRatingStats,
    updateThemeStats,
    updateAllStats
};


