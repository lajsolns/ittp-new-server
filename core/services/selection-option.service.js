const mongoose = require('mongoose');

const Region = mongoose.model('Region');
const Category = mongoose.model('Category');


exports.getRegions = async () => {
    const region = await Region.find({});
    console.log('region', region);
    return region;
};


exports.getCategories = () => {
    return Category.find({});
};


exports.editRegion = (params) => {
    const {id, label, order, value, description} = params;
    return Region.findByIdAndUpdate(id, {label, order, value, description});
};

exports.editCategory = (params) => {
    const {id, label, order, value, description} = params;
    return Category.findByIdAndUpdate(id, {label, order, value, description});
};

exports.deleteOneRegion = (id) => {
    return Region.deleteOne({_id: id})
};


exports.createRegion = (region) => {
    return new Region(region).save();
};


exports.getRegionById = (id) => {
    return Region.findById(id);
};


exports.getCategoryById = (id) => {
    return Category.findById(id);
};


exports.deleteOneCategory = (id) => {
    return Category.deleteOne({_id: id})
};


exports.createCategory = (category) => {
    return new Category(category).save();
};


