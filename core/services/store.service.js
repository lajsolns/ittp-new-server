const mongoose = require('mongoose');

const Theme = mongoose.model('Theme');
const asyncHandler = require('express-async-handler');

const themePreviewProjection = {'basicInfo': 1, 'settings': 1, stats: 1};


const mediaService = require('./media.service');

exports.getNewAndUpdated = () => {
};

exports.recent = () => {
};

exports.recommended = () => {
};

exports.getTopFree = () => {
    return Theme
        .find({}, themePreviewProjection)
        .where({'settings.price': {$eq: '0'}})
        .limit(8)
        .then(themes => {
            return themes.map(theme => getThemeStoreTileModel(theme));
        });
};

exports.getTopFreePaginated = asyncHandler(async (params) => {

    const {page, limit} = params;

    const myCustomLabels = {
        totalDocs: 'count',
        docs: 'data',
    };

    const qry = {$and: [{'settings.price': {$eq: '0'}}, {'settings.visibleInStore': true}]};

    const options = {
        page, limit,
        select: themePreviewProjection,
        customLabels: myCustomLabels,
        sort: {createdAt: -1}
    };

    const themes = await Theme.paginate(qry, options);
    const obj = {};
    obj.data = themes.data.map(theme => getThemeStoreTileModel(theme));
    obj.next = themes.hasNextPage;
    return obj;
});

exports.getTopPaid = () => {
    return Theme
        .find({}, themePreviewProjection)
        .where({'settings.price': {$gte: '1'}})
        .limit(8)
        .then(themes => {
            return themes.map(theme => getThemeStoreTileModel(theme));
        });
};


exports.getTopPaidPaginated = asyncHandler(async (params) => {

    const {page, limit} = params;

    const myCustomLabels = {
        totalDocs: 'count',
        docs: 'data',
    };

    const qry = {$and: [{'settings.price': {$gte: '1'}}, {'settings.visibleInStore': true}]};

    const options = {
        page, limit,
        select: themePreviewProjection,
        customLabels: myCustomLabels,
        sort: {createdAt: -1}
    };

    const themes = await Theme.paginate(qry, options);
    const obj = {};
    obj.data = themes.data.map(theme => getThemeStoreTileModel(theme));
    obj.next = themes.hasNextPage;
    return obj;
});

exports.getSimilarThemes = asyncHandler(async (themeId) => {
    const projection1 = {'settings.category': 1};
    const projection2 = {'basicInfo': 1, 'settings': 1, stats: 1};
    const theme = await Theme.findOne({_id: themeId}, projection1);
    const categories = theme.settings.category;
    const themes = await Theme.find({$and: [{'_id': {$ne: [themeId]}}, {'settings.visibleInStore': true}, {'settings.category': {$in: categories}}]}, projection2).limit(3);
    return getSimilarThemeModel(themes);
});

const getSimilarThemeModel = (data) => {
    return data.map(e => getModel(e));
};

const getModel = (e) => {
    const model = {};
    model._id = e._id;
    model.name = e.basicInfo.name;
    model.description = e.basicInfo.description;
    model.themeImageUrl = mediaService.getMediaUrls(e._id).themeIcon;
    model.createdBy = '';
    model.price = e.settings.price;
    model.rating = e.stats.ratings.average;
    return model;
};


exports.topNewFree = () => {
};

exports.topNewPaid = () => {
};

exports.getPaidThemePerCategory = asyncHandler(async (params) => {
    const {page, limit, categoryId} = params;

    const myCustomLabels = {
        totalDocs: 'count',
        docs: 'data',
    };

    const qry = {$and: [{'settings.price': {$gte: '1'}}, {'settings.visibleInStore': true}, {'settings.category': {$in: [categoryId]}}]};

    const options = {
        page, limit,
        select: themePreviewProjection,
        customLabels: myCustomLabels,
        sort: {createdAt: -1}
    };

    const themes = await Theme.paginate(qry, options);
    const obj = {};
    obj.data = themes.data.map(theme => getThemeStoreTileModel(theme));
    obj.next = themes.hasNextPage;
    return obj;
});

exports.getFreeThemePerCategory = asyncHandler(async (params) => {
    const {page, limit, categoryId} = params;

    const myCustomLabels = {
        totalDocs: 'count',
        docs: 'data',
    };

    const qry = {$and: [{'settings.price': {$eq: '0'}}, {'settings.visibleInStore': true}, {'settings.category': {$elemMatch: {$eq: categoryId}}}]};

    const options = {
        page, limit,
        select: themePreviewProjection,
        customLabels: myCustomLabels,
        sort: {createdAt: -1}
    };

    const themes = await Theme.paginate(qry, options);
    const obj = {};
    obj.data = themes.data.map(theme => getThemeStoreTileModel(theme));
    obj.next = themes.hasNextPage;
    return obj;
});


exports.similar = asyncHandler(async (themeId) => {
    const themes = await Theme.find({}, themePreviewProjection).limit(10);
    return themes.map(theme => getThemeStoreTileModel(theme));
});

exports.search = (q) => {
    return Theme
        .find({}, themePreviewProjection)
        .limit(8)
        .then(themes => {
            return themes.map(theme => getThemeStoreTileModel(theme));
        });
};


const getThemeStoreTileModel = (theme) => {
    const _id = theme._id.toString();
    const preview = {_id};
    preview.name = theme.basicInfo.name;
    preview.imageUrl = mediaService.getMediaUrls(_id).themeIcon;
    preview.price = theme.settings.price;
    preview.rating = theme.stats.ratings.average;
    return preview;
};

exports.getThemeStoreTileModel = getThemeStoreTileModel;


exports.search = asyncHandler(async (searchTerm) => {
    const themes = await Theme.find({
        "basicInfo.name": {
            "$regex": searchTerm,
            "$options": "i"
        },
        '_storage.deleted': false,
        'storeProperties.visibleInStore': true
    }, themePreviewProjection);

    return themes.map(theme => getThemeStoreTileModel(theme));
});


exports.getHomePageCategory = asyncHandler(async (params) => {
    const {page, limit, category, isPaid} = params;

    const myCustomLabels = {
        totalDocs: 'count',
        docs: 'data',
    };

    const options = {
        page, limit,
        select: themePreviewProjection,
        customLabels: myCustomLabels,
        sort: {updatedAt: -1}
    };

    const qry = {'settings.categories': category};
    qry['settings.price'] = isPaid ? {$ne: '0'} : '0';

    const themes = await Theme.paginate(qry, options);
    themes.data = themes.data.map(theme => getThemeStoreTileModel(theme));
    return themes;
});

