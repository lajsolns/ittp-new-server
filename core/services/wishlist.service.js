const mongoose = require('mongoose');

const statsService = require('./stats.service');
const storeService = require('./store.service');
const mediaService = require('./media.service');
const Theme = mongoose.model('Theme');
const Wishlist = mongoose.model('Wishlist');

exports.addThemeToUserWishlist = async ({themeId, userId}) => {
    const wishlist = await new Wishlist({themeId: themeId, userId: userId}).save();
    await statsService.updateWishlistCount(themeId);
    return wishlist;
};


exports.removeThemeFromUserWishlist = async ({themeId, userId}) => {
    const isDeleted = await Wishlist.deleteOne({themeId: themeId, userId: userId})
    await statsService.updateWishlistCount(themeId);
    return isDeleted;
};

exports.getWishListTheme = async (params) => {

    const {userId, page, limit} = params;

    const myCustomLabels = {
        totalDocs: 'count',
        docs: 'data',
    };


    const options = {
        page, limit,
        customLabels: myCustomLabels,
        sort: {createdAt: -1}
    };

    const wishlists = await Wishlist.find({"userId": userId});
    const idList = wishlists.map(wishlist => mongoose.Types.ObjectId(wishlist.themeId));
    const qry = {_id: {$in: idList}};
    // return Theme.find({_id: {$in: idList}});
    const themes = await Theme.paginate(qry, options);
    const obj = {};
    obj.data = themes.data.map(theme => storeService.getThemeStoreTileModel(theme));
    obj.next = themes.hasNextPage;

    return obj;
};


exports.isThemeOnUserWishlist = async ({themeId, userId}) => {
    const wishlist = await Wishlist.findOne({themeId, userId});
    return !!wishlist;
};


exports.getThemeStoreTileModel = (theme) => {
    const _id = theme._id.toString();
    const preview = {_id};
    preview.name = theme.basicInfo.name;
    preview.imageUrl = mediaService.getMediaUrls(_id).themeIcon;
    preview.price = theme.settings.price;
    preview.rating = theme.stats.ratings.average;

    return preview;
};
