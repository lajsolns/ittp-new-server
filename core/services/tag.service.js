const mongoose = require('mongoose');

const core = require('../../core');

const Tag = mongoose.model('Tag');

const saveSvc = require('./save.service');


exports.findTagById = (id) => {
    return Tag.findById(id);
};


exports.updateTag = (params) => {
    const {toSave, existing, user} = params;
    existing.location = toSave.location;
    existing.active = toSave.active;
    existing.propertyValues = toSave.propertyValues;
    return saveSvc.update({save: existing, user});
};


exports.createTag = (params) => {
    const {tag, themeId, user} = params;
    const newTag = new Tag(tag);
    newTag.themeId = themeId;
    return saveSvc.create({save: newTag, user});
};


exports.getTagsInBounds = (params) => {
    const {themeId, projection, bottomLeft, upperRight} = params;
    const $box = [
        [bottomLeft.longitude, bottomLeft.latitude],
        [upperRight.longitude, upperRight.latitude],
    ];

    return Tag.find({
        themeId,
        location: {$geoWithin: {$box}}
    }, projection).exec();
};

exports.getTagsCountByTheme = (themeId) => {
    return Tag.where({themeId: themeId}).countDocuments();
};



