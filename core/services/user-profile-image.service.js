const mongoose = require('mongoose');
const Jimp = require('jimp');
const path = require('path');

const core = require('../../core');

const UserProfileImage = mongoose.model('UserProfileImage');
const User = mongoose.model('User');

const defaultImageSize = 200;
const defaultImagePath = path.join(__dirname, 'default-icons', 'avatar.png');

exports.saveUserProfileImage = async (params) => {
    const {userId, filePath} = params;
    const image = await Jimp.read(filePath);
    core.unlinkAsync(filePath);
    image.contain(defaultImageSize, defaultImageSize);
    const data = await image.getBufferAsync(Jimp.MIME_PNG);
    await UserProfileImage.findOneAndUpdate({userId}, {data}, {upsert: true}).exec();
    const profileImageUrl = `${process.env.IMAGE_SERVER}/media/user/${userId}/profile-image`;
    await User.findByIdAndUpdate(userId, {profileImageUrl}).exec();
    return profileImageUrl;
};


exports.getUserProfileImage = async (params) => {
    const {userId, res} = params;
    const img = await UserProfileImage.findOne({userId}, {data: 1}).exec();
    if (img) {
        res.contentType('image/png');
        res.write(img.data);
        res.end();
    } else {
        res.sendFile(defaultImagePath);
    }
};
