const mongoose = require('mongoose');
const jimp = require('jimp');
const path = require('path');
const fs = require('fs');
const asyncHandler = require('express-async-handler');

const core = require('../../core');
const logger = require('../../utils/logger');

const Media = mongoose.model('Media');

const baseIconPath = path.join(__dirname, 'default-icons');
const defaultIcons = {
    'theme-icon': {
        filePath: (path.join(baseIconPath, 'theme-icon.png')),
        size: 200
    },
    'marker-64': {
        filePath: (path.join(baseIconPath, 'marker-64.png')),
        size: 64
    },
    'marker-48': {
        filePath: (path.join(baseIconPath, 'marker-48.png')),
        size: 64
    },
    'marker-32': {
        filePath: (path.join(baseIconPath, 'marker-32.png')),
        size: 64
    },
    'marker-24': {
        filePath: (path.join(baseIconPath, 'marker-24.png')),
        size: 64
    },
    'marker-16': {
        filePath: (path.join(baseIconPath, 'marker-16.png')),
        size: 64
    }
};

const allowedTypes = Object.keys(defaultIcons);
exports.allowedTypes = allowedTypes;

const saveThemeImage = async (params) => {
    const {themeId, type, filePath} = params;
    const size = defaultIcons[type].size;
    const image = await jimp.read(filePath);

    image.contain(size, size);
    const data = await image.getBufferAsync(jimp.MIME_PNG);

    fs.unlink(filePath, err => {
        if (err) {
            logger.error(err);
        }
    });

    return Media.findOneAndUpdate({themeId, type}, {data}, {upsert: true}).exec();
};


exports.findMediaByThemeIdAndType = async (params) => {
    const {themeId, type, res} = params;
    const img = await Media.findOne({themeId, type}, {data: 1}).exec();
            if (img) {
                res.contentType('image/png');
                res.write(img.data);
                res.end();
            } else {
                res.sendFile(defaultIcons[type].filePath);
            }
};


exports.getMediaUrls = (themeId) => {
    return {
        themeIcon: `${process.env.IMAGE_SERVER}/media/theme/${themeId}/theme-icon`,
        marker64: `${process.env.IMAGE_SERVER}/media/theme/${themeId}/marker-64`,
        marker48: `${process.env.IMAGE_SERVER}/media/theme/${themeId}/marker-48`,
        marker32: `${process.env.IMAGE_SERVER}/media/theme/${themeId}/marker-32`,
        marker24: `${process.env.IMAGE_SERVER}/media/theme/${themeId}/marker-24`,
        marker16: `${process.env.IMAGE_SERVER}/media/theme/${themeId}/marker-16`
    };
};


exports.saveMarkerIcons = (params) => {
    const {themeId, filePath} = params;
    return Promise
        .all([
            saveThemeImage({themeId, filePath, type: 'marker-64'}),
            saveThemeImage({themeId, filePath, type: 'marker-48'}),
            saveThemeImage({themeId, filePath, type: 'marker-32'}),
            saveThemeImage({themeId, filePath, type: 'marker-24'}),
            saveThemeImage({themeId, filePath, type: 'marker-16'}),
        ]);
};


exports.saveThemeIcon = (params) => {
    const {themeId, filePath} = params;
    return saveThemeImage({themeId, filePath, type: 'theme-icon'});
};


exports.saveThemeMedia = (params) => {
    const {themeId, filePath} = params;
    const promiseArray = allowedTypes.map(type => {
        return saveThemeImage({themeId, filePath, type});
    });

    return Promise.all(promiseArray);
};


