const lineReader = require('readline');
const fs = require('fs');

const Tag = require('mongoose').model('Tag');
const getColumnProcessors = require('./column-processors');
const lineProcessor = require('./line-processor');
const endHandler = require('./end-processing-handler');

module.exports = (params) => {
    const {tagOperation, tagProperties} = params;
    const startTime = Date.now();
    let processedLines = 0;
    let errorCount = 0;
    let errorMessages = '';
    const promiseList = [];

    const readerInterface = lineReader.createInterface({
        input: fs.createReadStream(tagOperation.fileInfo.path)
    });

    let columnProcessors;
    readerInterface.on('line', line => {
        const currentLineNum = ++processedLines;

        // get processors from line 1
        if (currentLineNum === 1) {
            columnProcessors = getColumnProcessors({line, tagProperties});
            return;
        }

        // skip the line for type fields
        if (currentLineNum === 2) {
            return;
        }

        const {hasErrors, errors, coordinates, propertyValues, _id}
            = lineProcessor({line, columnProcessors});

        if (hasErrors) {
            errorCount++;
            errorMessages += `Line ${currentLineNum} \n`;
            errors.forEach(x => {
                errorMessages += `\t${x}\n`;
            });
        } else {
            Tag
                .findById(_id)
                .then(tag => {
                    if (tag) {
                        tag.location.coordinates = coordinates;
                        tag._storage.updatedBy = tagOperation.userId;
                        tag.propertyValues = propertyValues;
                        promiseList.push(tag.save());
                    } else {
                        errorMessages += `Line ${currentLineNum} \n`;
                        errorMessages += `\ttag not found\n`;
                    }
                })
                .catch(console.log);
        }
    });

    readerInterface.on('close', () => {
        endHandler({
            promiseList, errorMessages,
            errorCount, startTime,
            tagOperation, processedLines
        });
    });

    readerInterface.on('error', () => {
        endHandler({
            promiseList, errorMessages,
            errorCount, startTime,
            tagOperation, processedLines
        });
    });
};
