const mongoose = require('mongoose');

const core = require('../../../core');
const TagOperation = mongoose.model('TagOperation');
const Tag = mongoose.model('Tag');

const MB_IN_KB = 1048576;

class FileColumnHeader {
    constructor(name, type, mandatory) {
        this.line1 = name;
        this.line2 = type;
        this.mandatory = mandatory;
    }

    getLine1String() {
        return this.line1 + (this.mandatory ? '*' : '') + '\t';
    }

    getLine2String() {
        return this.line2 + '\t';
    }
}


const fileColumnHeaderFromTagProperty = (tagProperty) => {
    const types = core.lib.types.tagPropertyType;
    const propType = types.find(x => x.value === tagProperty.tagPropertyType);
    return new FileColumnHeader(tagProperty.name, propType.label, tagProperty.mandatory);
};


const fileColumnHeadersForTagTemplate = (theme) => {
    const latHeader = new FileColumnHeader('Latitude', 'Numbers Only', true);
    const lonHeader = new FileColumnHeader('Longitude', 'Numbers Only', true);
    const list = [latHeader, lonHeader];
    theme.tagProperties.forEach(x => {
        list.push(fileColumnHeaderFromTagProperty(x));
    });
    return list;
};


const fileColumnHeadersForExistingTagsFile = (theme) => {
    const idHeader = new FileColumnHeader('Id', 'Do not modify!', true);
    const latHeader = new FileColumnHeader('Latitude', 'Numbers Only', true);
    const lonHeader = new FileColumnHeader('Longitude', 'Numbers Only', true);
    const list = [idHeader, latHeader, lonHeader];
    theme.tagProperties.forEach(x => {
        list.push(fileColumnHeaderFromTagProperty(x));
    });
    return list;
};


const getFileName = (name, type) => {
    return `${name}-${type}-${Date.now()}.txt`.replace(/\s/g,'_');
};


const getTagTemplateString = (theme) => {
    let line1 = '';
    let line2 = '';

    fileColumnHeadersForTagTemplate(theme).forEach(x => {
        line1 += x.getLine1String();
        line2 += x.getLine2String();
    });

    return (line1 + '\r\n' + line2 + '\r\n');
};


const getExistingTagFileHeaderModel = (theme) => {
    let line1 = '';
    let line2 = '';
    const headers = fileColumnHeadersForExistingTagsFile(theme);

    headers.forEach(x => {
        line1 += x.getLine1String();
        line2 += x.getLine2String();
    });

    const headerString = (line1 + '\r\n' + line2 + '\r\n');
    return {headers, headerString};
};

const operationStatus = {processing: 'PROCESSING', done: 'DONE'};
const jobTypes = {newTags: 'NEW-TAGS', updateTags: 'UPDATE-TAGS'};


exports.getFileOperationsForTheme = (themeId) => {
    return TagOperation.find({themeId}, {}, {sort: {updatedAt: -1}});
};


exports.insertNewTagsOperation = (params) => {
    const {themeId, userId, name, size, path} = params;
    const sizeInMB =  (size / MB_IN_KB).toFixed(2);
    const newOperation = new TagOperation();
    newOperation.userId = userId;
    newOperation.jobType = jobTypes.newTags;
    newOperation.themeId = themeId;
    newOperation.fileInfo = {name, size: sizeInMB, path};
    newOperation.status = operationStatus.processing;
    return newOperation.save();
};


exports.insertUpdatedTagsOperation = (params) => {
    const {themeId, userId, name, size, path} = params;
    const sizeInMB = (size / MB_IN_KB).toFixed(2);
    const newOperation = new TagOperation();
    newOperation.userId = userId;
    newOperation.jobType = jobTypes.updateTags;
    newOperation.themeId = themeId;
    newOperation.fileInfo = {name, size: sizeInMB, path};
    newOperation.status = operationStatus.processing;
    return newOperation.save();
};


exports.generateTagTemplateFile = (theme) => {
    const filename = getFileName(theme.basicInfo.name, 'template');
    const content = getTagTemplateString(theme);
    return {filename, content}
};


exports.generateExistingTagsFile = (params) => {
    const {theme, res, next} = params;
    const themeId = theme._id.toString();

    const filename = getFileName(theme.basicInfo.name, 'tags');
    const headersModel = getExistingTagFileHeaderModel(theme);

    res.writeHead(200, {
        'Content-Type': 'application/octet-stream',
        'Content-Disposition': `attachment;filename=${filename}`
    });

    // first 2 lines of column headers
    res.write(headersModel.headerString);

    // each tag forms one line
    Tag.find({themeId}).cursor().eachAsync(tag => {
        // write id, lat, lon
        res.write(`${tag._id}\t${tag.location.coordinates[1]}\t${tag.location.coordinates[0]}\t`);
        theme.tagProperties.forEach(prop => {
            const currentValue = tag.propertyValues.find(x => {
                return x.normalizedName === prop.normalizedName;
            });
            if (currentValue && currentValue.value) {
                params.res.write(`${currentValue.value}\t`);
            } else {
                params.res.write(`${''}\t`);
            }
        });

        params.res.write('\r\n');
    })
        .then(result => res.end())
        .catch(next);
};

