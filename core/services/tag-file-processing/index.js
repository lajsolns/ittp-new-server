exports.operations = require('./file-operations');

const processors = {};
processors.new = require('./new-tags-processor');
processors.update = require('./update-tag-processor');

exports.processors = processors;

