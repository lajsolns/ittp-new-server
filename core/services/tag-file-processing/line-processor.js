module.exports = (params) => {
    const {columnProcessors, line} = params;
    const valueColumns = line.trim().split('\t').map(x => x.trim());
    const {idProcessor, latitudeProcessor, longitudeProcessor, propProcessors} = columnProcessors;
    const errors = [];

    const _id = valueColumns[idProcessor.index];

    const latitude = valueColumns[latitudeProcessor.index];
    if (!latitudeProcessor.isValid(latitude)) {
        errors.push(`<br>Column ${latitudeProcessor.column} Latitude is invalid. Must be a decimal number between -90 and 90`);
    }

    const longitude = valueColumns[longitudeProcessor.index];
    if (!longitudeProcessor.isValid(longitude)) {
        errors.push(`<br>Column ${longitudeProcessor.column} Longitude is invalid. Must be a decimal number between -180 and 180`);
    }

    const propertyValues = [];
    propProcessors.forEach(processor => {
        const {index, mandatory, normalizedName, name, isValid, column, tagPropertyType} = processor;
        const value = valueColumns[index];

        if (hasValue(value)) {
            if (isValid(value)) {
                propertyValues.push({value, normalizedName, tagPropertyType});
            } else {
                errors.push(`<br>Column ${column} ${name} is invalid`);
            }
        } else {
            if (mandatory) {
                errors.push(`<br>Column ${column} ${name} is required`);
            } else {
                propertyValues.push({value, normalizedName, tagPropertyType});
            }
        }
    });

    const hasErrors = !!errors.length;
    return {
        hasErrors, errors, _id,
        coordinates: [longitude, latitude],
        propertyValues
    };
};


function hasValue(value) {
    return (value !== null && value !== undefined && value !== '')
}