const typeValidator = require('./property-type-validators');

const isValidLatitude = function (lat) {
    return !(lat < -90 || lat > 90);
};

const isValidLongitude = function (lon) {
    return !(lon < -180 || lon > 180);
};

module.exports = (params) => {
    const {line, tagProperties} = params;
    const valueColumns = line.trim().split('\t').map(x => x.trim());

    const idProcessor = {};
    idProcessor.name = 'Id';
    idProcessor.index = valueColumns.indexOf('Id*');
    idProcessor.column = idProcessor + 1;

    const latitudeProcessor = {};
    latitudeProcessor.index = valueColumns.indexOf('Latitude*');
    latitudeProcessor.isValid = isValidLatitude;
    latitudeProcessor.column = latitudeProcessor + 1;
    latitudeProcessor.name = 'Latitude';

    const longitudeProcessor = {};
    longitudeProcessor.index = valueColumns.indexOf('Longitude*');
    longitudeProcessor.isValid = isValidLongitude;
    longitudeProcessor.column = longitudeProcessor + 1;
    latitudeProcessor.name = 'Longitude';

    const processors = {
        idProcessor,
        latitudeProcessor,
        longitudeProcessor,
        propProcessors: []
    };

    tagProperties.forEach(prop => {
        const {tagPropertyType, name, mandatory, normalizedName} = prop;
        const columnName = mandatory ? `${name}*` : name;
        const index = valueColumns.indexOf(columnName);
        const column = index + 1;
        const isValid = (value) => {
            return typeValidator(tagPropertyType, value);
        };
        processors.propProcessors.push({index, isValid, column, mandatory, name, normalizedName, tagPropertyType});
    });

    return processors;
};
