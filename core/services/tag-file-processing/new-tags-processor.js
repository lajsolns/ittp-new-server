const lineReader = require('readline');
const fs = require('fs');

const Tag = require('mongoose').model('Tag');
const getColumnProcessors = require('./column-processors');
const lineProcessor = require('./line-processor');
const endHandler = require('./end-processing-handler');


module.exports = (params) => {
    const {tagOperation, tagProperties} = params;
    const startTime = Date.now();
    let processedLines = 0;
    let errorCount = 0;
    let errorMessages = '';
    const promiseList = [];

    const readerInterface = lineReader.createInterface({
        input: fs.createReadStream(tagOperation.fileInfo.path)
    });

    let columnProcessors;
    readerInterface.on('line', line => {
        const currentLineNum = ++processedLines;

        // get processors from line 1
        if (currentLineNum === 1) {
            columnProcessors = getColumnProcessors({line, tagProperties});
            return;
        }

        // skip the line for type fields
        if (currentLineNum === 2) {
            return;
        }

        const {hasErrors, errors, coordinates, propertyValues}
            = lineProcessor({line, columnProcessors});

        if (hasErrors) {
            errorCount++;
            errorMessages += `<p>Line ${currentLineNum} ${errors.join(' ')}</p>`;
        } else {
            const tag = new Tag({propertyValues});
            tag.themeId = tagOperation.themeId;
            tag.location.coordinates = coordinates;
            tag._storage.createdBy = tagOperation.userId;
            tag._storage.updatedBy = tagOperation.userId;
            promiseList.push(tag.save());
        }
    });

    readerInterface.on('close', () => {
        endHandler({
            promiseList, errorMessages,
            errorCount, startTime,
            tagOperation, processedLines
        });
    });

    readerInterface.on('error', () => {
        endHandler({
            promiseList, errorMessages,
            errorCount, startTime,
            tagOperation, processedLines
        });
    });
};
