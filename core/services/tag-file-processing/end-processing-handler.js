const fs = require('fs');
const statService = require('../stats.service');

module.exports = (params) => {
    const {
        promiseList, errorMessages,
        errorCount, startTime,
        tagOperation, processedLines
    } = params;

    Promise
        .all(promiseList.map(x => x.reflect()))
        .then(allPromises => {

            let savedTagsCount = 0;
            allPromises.forEach(result => {
                if (result.isFulfilled()) {
                    savedTagsCount++;
                } else {
                    console.log(result.reason());
                }
            });

            const duration = ((Date.now() - startTime) / 1000).toFixed(2);
            tagOperation.status = 'DONE';
            tagOperation.hasErrors = errorCount > 0;
            tagOperation.summary = {
                processedLines, savedTagsCount,
                errorCount, errorMessages,
                duration,
            };

            tagOperation
                .save()
                .then(result => console.log('tags file processing complete', result));

            fs.unlink(tagOperation.fileInfo.path, err => {
                if (err) {
                    console.log('tags delete error', err);
                }
            });

            statService.updateTagStats(tagOperation.themeId);
        })
        .catch(console.log);
};
