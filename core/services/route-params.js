const themeService = require('./theme.service');
const tagService = require('./tag.service');
const userService = require('./user.service');
const deviceService = require('./device.service');
const asyncHandler = require('express-async-handler');
exports.themeIdParam = (req, res, next, themeId) => {
    themeService
        .findThemeById(themeId)
        .then(theme => {
            if (theme) {
                req.context.theme = theme;
                req.context.themeId = themeId;
                req.context.themeName = theme.basicInfo.name;
                next();
            } else {
                res.status(404).send({message: 'Theme not found'});
            }
        })
        .catch(err => next(err))
};


exports.tagIdParam = (req, res, next, tagId) => {
    tagService
        .findTagById(tagId)
        .then(tag => {
            if (tag) {
                req.context.tag = tag;
                req.context.tagId = tagId;
                next();
            } else {
                res.status(404).send({message: 'Tag not found'});
            }
        })
        .catch(err => next(err))
};


exports.userIdParam = asyncHandler(async (req, res, next) => {
    const userId = req.params.userId;
    const user = await userService.findUserById(userId);
    if (user) {
        req.context.user = user;
        req.context.userId = userId;
        next();
    } else {
        res.status(404).send({message: 'User not found'});
    }
});


exports.deviceIdParam = asyncHandler(async (req, res, next) => {
    const deviceId = req.params.deviceId;
    const device = await deviceService.findDeviceById(deviceId);
    if (device) {
        req.context.device = device;
        req.context.deviceId = deviceId;
        next();
    } else {
        res.status(404).send({message: 'Device not found'});
    }
});


exports.userInfoRouteGuard = asyncHandler(async (req, res, next) => {
    const user = await userService.findUserById(req.user._id);
    if (user) {
        req.user.isAdmin = user.isAdmin;
        next();
    } else {
        res.status(404).send({message: 'User not found'});
    }

});
