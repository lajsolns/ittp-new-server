exports.create = ({save, user}) => {
    save._storage.createdBy = user.name;
    save._storage.createdById = user._id;
    save._storage.updatedBy = user.name;
    save._storage.updatedById = user._id;
    return save.save();
};

exports.update = ({save, user}) => {
    save._storage.updatedBy = user.name;
    save._storage.updatedById = user._id;
    return save.save();
};
