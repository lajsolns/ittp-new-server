const mongoose = require('mongoose');

const AccessList = mongoose.model('AccessList');
const Theme = mongoose.model('Theme');

exports.addUserToList = ({themeId, userId}) => {
    return new AccessList({themeId: themeId, userId: userId}).save();
};


exports.removeUserFromList = ({themeId, userId}) => {
    return AccessList.deleteOne({themeId: themeId, userId: userId});
};


const isUserOnList = async ({themeId, userId}) => {
    const doc = await AccessList.findOne({themeId, userId});
    return !!doc;
};


exports.hasAccess = async ({themeId, userId}) => {
    const theme = await Theme.findById(themeId);

    if (theme.settings.useAccessList) {
        return isUserOnList({themeId, userId});
    } else {
        return true;
    }
};

