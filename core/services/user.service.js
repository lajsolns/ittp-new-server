const mongoose = require('mongoose');

const User = mongoose.model('User');

const themeSvc = require('./theme.service');
const subscriptionSvc = require('./subscription.service');
const deviceSvc = require('./device.service');
const mediaSvc = require('./media.service');


exports.findUserById = (id) => {
    return User.findById(id);
};


exports.getAllUsersPaginate = async (params) => {
    const {page, limit} = params;

    const myCustomLabels = {
        totalDocs: 'count',
        docs: 'data',
    };

    const options = {
        page, limit,
        select: {name: 1, profileImageUrl: 1},
        customLabels: myCustomLabels,
        sort: {updatedAt: -1}
    };

    const qry = {};

    const users = await User.paginate(qry, options);
    users.data = users.data.map(user => {
        const obj = {};
        obj._id = user._id;
        obj.name = user.name;
        obj.profileImageUrl = user.profileImageUrl;
        return obj;
    });
    return users;
};


exports.getUserDevices = deviceSvc.findUserDevices;


exports.getUserCreatedThemes = themeSvc.getUserCreatedThemes;


exports.getUserSubscribedThemes = async (userId) => {
    const themeIdArray = await subscriptionSvc.getUserSubscriptions(userId);
    const themes = await themeSvc.findThemesInIdArray({list: themeIdArray, projection: {basicInfo: 1}});

    return themes.map(theme => {
        const id = theme._id.toString();
        const imageUrl = mediaSvc.getMediaUrls(id).themeIcon;
        const name = theme.basicInfo.name;
        return {id, imageUrl, name};
    });
};


exports.getAdminUsersPaginate = async (params) => {
    const {page, limit} = params;
    const myCustomLabels = {
        totalDocs: 'count',
        docs: 'data',
    };

    const options = {
        page, limit,
        select: {email: 1, profileImageUrl: 1, name: 1},
        customLabels: myCustomLabels,
        sort: {updatedAt: -1}
    };

    const qry = {isAdmin: true};

    const users = await User.paginate(qry, options);
    users.data = users.data.map(user => {
        const obj = {};
        obj._id = user._id;
        obj.email = user.email;
        obj.profileImageUrl = user.profileImageUrl;
        obj.username = user.name;
        return obj;
    });
    return users;
};


exports.searchUser = (searchTerm) => {
    return User
        .find({
            "email": {
                "$regex": searchTerm,
                "$options": "i"
            }
        });
};

exports.getUser = (id) => {
    return User.findOne({'email': id}, {'email': 1});
};

exports.searchAdminUser = (searchTerm) => {
    return User
        .find({
            "email": {
                "$regex": searchTerm,
                "$options": "i"
            },
            "isAdmin": true
        });
};


exports.addAddAdminUser = function (id) {
    return User.findByIdAndUpdate(id, {isAdmin: true});
};


exports.removeAdminUser = function (id) {
    return User.findByIdAndUpdate(id, {isAdmin: false});
};


exports.getNameFromUserId = async (id) => {
    const user = await User.find({_id: id});
    return user.name;
};

//
// exports.findLocalUserByEmail = (email) => {
//     return User.findOne({email});
// };


exports.findUsersInIdArray = (params) => {
    const {list, projection} = params;
    const idList = list.map(id => mongoose.Types.ObjectId(id));
    const qry = {_id: {$in: idList}};
    return User.find(qry, projection);
};
