const mongoose = require('mongoose');
const asyncHandler = require('express-async-handler');
const jwt = require('jsonwebtoken');
const blueBird = require("bluebird");

const core = require('../../core');
const logger = require('../../utils/logger');

const Subscription = mongoose.model('Subscription');
const Device = mongoose.model('Device');

const DeviceLocation = mongoose.model('DeviceLocation');

const Tag = mongoose.model('Tag');

const themeSvc = require('./theme.service');
const mediaSvc = require('./media.service');

function findSubscriptionsForUser({userId, themeIdList}) {
    return Subscription.find(
        {
            userId, active: true,
            themeId: {$in: themeIdList}
        },
        {createdAt: 1, themeId: 1}
    );
}


function getAppThemeModel({theme, subscribedAt}) {
    const model = {subscribedAt};
    model._id = theme._id.toString();
    model.name = theme.basicInfo.name;
    model.description = theme.basicInfo.description;
    model.color = theme.basicInfo.color;
    model.createdAt = theme.createdAt;
    model.createdBy = theme._storage.createdBy;
    model.tagProperties = theme.tagProperties;
    model.defaultNotificationDistance = theme.basicInfo.defaultDistanceMeters;
    model.defaultSilence = theme.basicInfo.defaultTagSilenceSeconds;
    model.themeIconUrl = mediaSvc.getMediaUrls(model._id).themeIcon;
    // model.mediaUrls = mediaSvc.getMediaUrls(model._id);
    return model;
}


exports.getThemes = async ({userId, themeIdList}) => {
    const result = await Promise.all([
        themeSvc.findThemesInIdArray({list: themeIdList, projection: {basicInfo: 1, tagProperties: 1}}),
        findSubscriptionsForUser({userId, themeIdList})
    ]);
    const [themes, subs] = result;
    return themes.map(theme => {
        const subscribedAt = subs.find(x => x.themeId === theme._id.toString()).createdAt;
        return getAppThemeModel({theme, subscribedAt});
    });
};


exports.getTags = async (ids) => {
    const realIds = ids.map(x => mongoose.Types.ObjectId(x));
    const tags = await Tag.find({_id: {$in: realIds}});
    return tags.map(x => {
        const obj = {};
        obj._id = x._id.toString();
        obj.themeId = x.themeId;
        obj.__v = x.__v;
        const [longitude, latitude] = x.location.coordinates;
        obj.latitude = latitude;
        obj.longitude = longitude;

        obj.createdBy = x._storage.createdBy;
        obj.createdAt = x.createdAt;

        obj.updatedAt = x.updatedAt;
        obj.updatedBy = x._storage.updatedBy;

        return obj;
    });
};


exports.getTagsNearLocation = async ({models, location}) => {
    const blueBirdPmsArr = [];

    models.forEach(model => {
        const coordinates = [location.longitude, location.latitude];
        model.location = {type: 'Point', coordinates};
        const queryPms = getTagsNearLocationForOneTheme(model);
        const blueBirdPms = blueBird.resolve(queryPms).reflect();
        blueBirdPmsArr.push(blueBirdPms);
    });

    const results = await Promise.all(blueBirdPmsArr);
    const successArr = [];
    results.forEach(pms => {
        if (pms.isFulfilled()) {
            successArr.push(pms.value())
        }
    });
    return flatten(successArr);
};

function flatten(arr) {
    return arr.reduce(function (flat, toFlatten) {
        return flat.concat(Array.isArray(toFlatten) ? flatten(toFlatten) : toFlatten);
    }, []);
}

async function getTagsNearLocationForOneTheme({themeId, maxDistance, location}) {
    const tags = await Tag.find({
        themeId,
        '_storage.deleted': false,
        location: {
            $near: {
                $geometry: location,
                $maxDistance: maxDistance
            }
        }
    }, {__v: 1}).exec();
    return getTagVersion(tags);
}


exports.getThemeTagsInBox = async ({bounds, themeIds} ) => {
    const {southwest, northeast} = bounds;
    const box = [[southwest.longitude, southwest.latitude], [northeast.longitude, northeast.latitude]];
    const tags = await Tag.find({
        themeId: {$in: themeIds},
        location: {$geoWithin: {$box: box}}
    }, {__v: 1});
    return getTagVersion(tags);
};


function getTagVersion(tags) {
    return tags.map(tag => {
        const obj = {};
        obj.__v = tag._doc.__v;
        obj._id = tag._doc._id.toString();
        return obj;
    });
}


exports.deviceTokenHeaderParam = asyncHandler(async (req, res, next) => {
    const token = req.header('device-token');
    if (!token) {
        return next({statusCode: 400, errMessage: 'device token missing'});
    }
    const decoded = jwt.verify(token, process.env.SECRET);
    const device = await Device.findById(decoded.id);
    req.context.deviceId = decoded.id;
    next();
});


exports.saveDeviceLocation = (locations) => {
    const deviceLocs = locations.map(x => new DeviceLocation(x));
    return DeviceLocation.insertMany(deviceLocs);
};
