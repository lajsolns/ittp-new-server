const mongoose = require('mongoose');
const paypal = require('paypal-rest-sdk');

const ThemeBillingPlan = mongoose.model('ThemeBillingPlan');
const Payment = mongoose.model('Payment');
const Theme = mongoose.model('Theme');

paypal.configure({
    mode: process.env.PAYPAL_MODE, //sandbox or live
    client_id: process.env.PAYPAL_CLIENT_ID,
    client_secret: process.env.PAYPAL_CLIENT_SECRET
});


const paypalBillingAgreementCreate = (params) => {
    return new Promise((resolve, reject) => {
        paypal.billingAgreement.create(params, (error, billingAgreement) => {
            if (error) {
                return reject(error);
            }

            const approvalUrl = billingAgreement.links.find(link => link.rel === 'approval_url').href;
            resolve({approvalUrl});
        });
    });
};

const paypalBillingAgreementExecute = (token) => {
    return new Promise((resolve, reject) => {
        paypal.billingAgreement.execute(token, function (error, payment) {
            if (error) {
                return reject(error);
            }
            return resolve(payment);
        });
    });
};

const paypalBillingAgreementCancel = (id) => {
    return new Promise((resolve, reject) => {
        paypal.billingAgreement.cancel(id, {note: 'unsubscribe'}, function (error, response) {
            if (error) {
                return reject(error);
            }

            return resolve(response);
        });
    });
};

const paypalBillingPlanActivate = (id) => {
    return new Promise((resolve, reject) => {
        paypal.billingPlan.activate(id, (error) => {
            if (error) {
                return reject(error);
            }

            return resolve();
        });
    });
};

const paypalBillingPlanCreate = (params) => {
    return new Promise((resolve, reject) => {
        paypal.billingPlan.create(params, (error, plan) => {
            if (error) {
                return reject(error);
            }

            return resolve(plan);
        });
    });
};


exports.createBillingAgreement = async (params) => {
    const {themeName, themeDescription, billingPlanId} = params;
    const next15MinsMillis = Date.now() + (15 * 60 * 1000);
    const next15MinsIsoStr = new Date(next15MinsMillis).toISOString();
    const billingAgreementParams = {
        name: themeName,
        description: themeDescription,
        start_date: next15MinsIsoStr,
        plan: {id: billingPlanId},
        payer: {payment_method: "paypal"},
    };
    return await paypalBillingAgreementCreate(billingAgreementParams);
};


const executeBillingAgreement = async ({userId, token, billingPlan}) => {
    const payment = await paypalBillingAgreementExecute(token);
    const newPayment = new Payment();
    newPayment.themeId = billingPlan.themeId;
    newPayment.createdBy = userId;
    newPayment.userId = userId;
    newPayment.payerInfo = getPayerInfoFromPaymentResponse(payment);
    newPayment.details = getDetailsFromPaymentResponseAndBillingPlan(payment, billingPlan);
    return newPayment.save();
};
exports.executeBillingAgreement = executeBillingAgreement;


function getPayerInfoFromPaymentResponse(payment) {
    const payerInfo = {};
    payerInfo.id = payment.payer.payer_info.payer_id;
    payerInfo.firstName = payment.payer.payer_info.first_name;
    payerInfo.lastName = payment.payer.payer_info.last_name;
    payerInfo.email = payment.payer.payer_info.email;
    payerInfo.paymentMethod = payment.payer.payment_method;
    return payerInfo;
}

function getDetailsFromPaymentResponseAndBillingPlan(payment, billingPlan) {
    const details = {};
    details.billingPlanId = billingPlan._id.toString();
    details.billingAgreementId = payment.id;
    details.price = billingPlan.price;
    details.currency = billingPlan.currency;
    details.selfLink = payment.links.find(link => link.rel === 'self').href;
    return details;
}


const createPaypalBillingPlan = async (params) => {
    const {themeId, themeName, price, currency} = params;
    const billingPlanParams = {
        name: 'iTagTip Theme Subscription Plan',
        description: themeId,
        type: "INFINITE",
        payment_definitions: [
            {
                name: `${themeName} Subscription`,
                type: "REGULAR",
                frequency: "MONTH",
                frequency_interval: "1",
                amount:
                    {
                        value: price,
                        currency: currency,
                    },
                cycles: "0",

            }],
        merchant_preferences:
            {
                setup_fee:
                    {
                        value: "0",
                        currency: 'USD',
                    },
                return_url: `${process.env.STORE_ROOT}/paypal-approval/${themeId}`,
                cancel_url: `${process.env.STORE_ROOT}/store-details/${themeId}`,
                auto_bill_amount: "YES",
                initial_fail_amount_action: "CONTINUE",
                max_fail_attempts: "0"
            }
    };
    const plan = await paypalBillingPlanCreate(billingPlanParams);
    await paypalBillingPlanActivate(plan.id);
    const selfLink = plan.links.find(link => link.rel === 'self').href;
    const themeBillingPlan = new ThemeBillingPlan({
        price, currency,
        themeId, selfLink,
        billingPlanId: plan.id
    });
    return themeBillingPlan.save();
};


const createZeroBillingPlan = (themeId) => {
    const themeBillingPlan = new ThemeBillingPlan({
        price: '0',
        currency: 'USD',
        themeId
    });

    return themeBillingPlan.save();
};


exports.updateThemeBillingPlan = async ({oldPrice, newPrice, themeId, themeName, newCurrency, oldCurrency}) => {
    const isPriceZero = parseFloat(newPrice) === 0;
    if (isPriceZero) {
        return await createZeroBillingPlan(themeId);
    } else {
        return await createPaypalBillingPlan({
            themeId,
            themeName,
            price: newPrice,
            currency: newCurrency
        });
    }
};


const getUserLatestActivePaymentForTheme = (params) => {
    const {themeId, userId} = params;
    return Payment.findOne({userId, themeId, active: true}, {}, {sort: {createdAt: -1}});
};

exports.getUserLatestActivePaymentForTheme = getUserLatestActivePaymentForTheme;


async function cancelBillingAgreement(payment) {
    await paypalBillingAgreementCancel(payment.details.billingAgreementId, {note: 'unsubscribe'});
    payment.canceledAt = Date.now();
    payment.active = false;
    return payment.save();
}


function findUserActivePaymentsForTheme(params) {
    const {themeId, userId} = params;
    return Payment.find({userId, themeId, active: true});
}


exports.findUserActivePaymentsForTheme = findUserActivePaymentsForTheme;


exports.cancelAllUserActivePaymentsForTheme = async (params) => {
    const payments = await findUserActivePaymentsForTheme(params);
    const pmsArr = payments.map(payment => cancelBillingAgreement(payment));
    return Promise.all(pmsArr);
};


exports.getUserPayments = (userId) => {
    return Payment.find({userId}, {}, {sort: {createdAt: -1}})
};


exports.getUserPaymentsPaginated = async ({userId, page, limit}) => {
    const myCustomLabels = {
        totalDocs: 'count',
        docs: 'data',
    };
    const options = {
        page, limit,
        customLabels: myCustomLabels,
        sort: {createdAt: -1}
    };
    const qry = {userId};
    const result = await Payment.paginate(qry, options);
    result.data = result.data.map(payment => {
        const obj = {};
        obj._id = payment.payerInfo.id;
        obj.firstName = payment.payerInfo.firstName;
        obj.lastName = payment.payerInfo.lastName;
        obj.email = payment.payerInfo.email;
        obj.paymentMethod = payment.payerInfo.paymentMethod;
        obj.billingPlan = payment.details.billingPlanId;
        obj.billingAgreement = payment.details.billingAgreementId;
        obj.price = payment.details.price;
        obj.currency = payment.details.currency;
        return obj;
    });
    return result;
};


exports.getThemePayments = (themeId) => {
    return Payment.find({themeId}, {}, {sort: {createdAt: -1}})
};


const getLatestThemeBillingPlan = (themeId) => {
    return ThemeBillingPlan.findOne({themeId}, {}, {sort: {createdAt: -1}});
};

exports.getLatestThemeBillingPlan = getLatestThemeBillingPlan;


exports.isSubscriptionPaymentRequired = async ({themeId, userId}) => {
    const theme = await Theme.findById(themeId);
    if (theme.isPaidTheme()) {
        const payment = await getUserLatestActivePaymentForTheme({themeId, userId});
        if (payment) {
            return {required: false, paymentId: payment._id.toString()};
        } else {
            return {required: true};
        }
    } else {
        return {required: false};
    }
};


const cancelUserSubscriptionPayment = async (paymentId) => {
    const payment = await Payment.findById(paymentId);
    if (payment.active) {
        await cancelBillingAgreement(payment.details.billingAgreementId, {note: 'unsubscribe'});
        payment.canceledAt = Date.now();
        payment.active = false;
        return payment.save();
    }
};

exports.cancelUserSubscriptionPayment = cancelUserSubscriptionPayment;

exports.executePayment = async ({userId, themeId, token}) => {
    const payment = await getUserLatestActivePaymentForTheme({userId, themeId});
    if (payment) {
        await cancelUserSubscriptionPayment(payment._id.toString());
        return await tryExecuteAgreement();
    } else {
        return await tryExecuteAgreement();
    }

    async function tryExecuteAgreement() {
        const billingPlan = await getLatestThemeBillingPlan(themeId);
        return await executeBillingAgreement({userId, token, billingPlan});
    }
};



