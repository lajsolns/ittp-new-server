const azure = require('azure-sb');

const lib = require('../lib');
const {logger} = lib;

const hub = azure.createNotificationHubService(
    process.env.AZURE_HUB_NAME, process.env.AZURE_HUB_ENDPOINT
);


const updateDeviceTags = ({regId, token, tags}) => {
    return new Promise((resolve, reject) => {
        hub.gcm
            .createOrUpdateNativeRegistration(regId, token, tags, (err, res) => {
                if (err) {
                    logger.error('native registration failed');
                    return reject(err);
                }

                logger.info('full registration complete');
                return resolve(res);
            });
    });
};


exports.registerAndroidDevice = ({token, tags}) => {
    return new Promise((resolve, reject) => {
        hub.createRegistrationId((err, regId) => {
            if (err) {
                return reject(err);
            }

            updateDeviceTags({regId, token, tags})
                .then(result => resolve(regId))
                .catch(reject);
        });
    });
};


exports.updateDeviceTags = updateDeviceTags;


const sendMessageToAndroidDevice = ({tag, data}) => {
    return new Promise((resolve, reject) => {
        hub.gcm
            .send(tag, {data}, (err, res) => {
                if (err) {
                    logger.error('message sending failed');
                    return reject(err);
                }

                logger.info('message sent');
                return resolve(res);
            });
    });
};
exports.sendMessageToAndroidDevice = sendMessageToAndroidDevice;

exports.notifySubscriptionChange = (userId) => {
    const tag = userId;
    const data = {code: 'SUBSCRIPTION_CHANGE'};
    return sendMessageToAndroidDevice({tag, data})
};


exports.notifyThemeChange = (themeId) => {
    const tag = themeId;
    const data = {code: 'THEME_CHANGE', content: themeId};
    return sendMessageToAndroidDevice({tag, data})
};
