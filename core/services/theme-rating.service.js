const mongoose = require('mongoose');
const Review = mongoose.model('Review');
const themeStats = require('./stats.service');

exports.createReview = (review) => {
    return new Review(review).save()
        .then(result => {
            return themeStats.updateRatingStats(review.themeId);
        });
};

exports.updateReview = async (params) => {
    const {message, themeId, rating, userId} = params;
    const qry = {themeId, userId};
    await Review.findOneAndUpdate(qry, {$set: {message: message, rating: rating}}, {new: true});
    return themeStats.updateRatingStats(themeId);
};

exports.getReviewById = (id) => {
    return Review.find({themeId: id}).sort('createdAt');
};

exports.getReviewByUserId = (id, userId) => {
    return Review.findOne({themeId: id, userId: userId});
};


exports.getAllReviewsPaginate = async (params) => {
    const {page, limit, themeId} = params;

    const myCustomLabels = {
        totalDocs: 'count',
        docs: 'data',
    };

    const options = {
        page, limit,
        select: {message: 1, rating: 1, name: 1, profileImageUrl: 1, createdAt: 1},
        customLabels: myCustomLabels,
        sort: {createdAt: 1}
    };

    const qry = {themeId};

    const reviews = await Review.paginate(qry, options);
    reviews.data = reviews.data.map(review => {
        const obj = {};
        obj._id = review._id;
        obj.themeId = review.themeId;
        obj.message = review.message;
        obj.rating = review.rating;
        obj.name = review.name;
        obj.profileImageUrl = review.profileImageUrl;
        return obj;
    });
    return reviews;
};

exports.getReviewRatings = async (param) => {
    const ratings = await Review.aggregate([
        {$match: {'themeId': param}},
        {
            $group: {
                _id: '$rating',
                count: {$sum: 1},
                average: {$avg: '$rating'}
            }
        },
        {$sort: {count: -1}}
    ]);
    return ratings;
};

