const nodemailer = require("nodemailer");
const xoauth2 = require("xoauth2");
const mongoose = require('mongoose');
const User = mongoose.model('User');


exports.sendMail = async (themeId, userId) => {

    const email = await getUserEmail(userId);
    

    let transporter = await nodemailer.createTransport({
        host: 'smtp.gmail.com',
        auth: {
            type: 'OAuth2',
                user: 'itagtip.test@gmail.com',
                clientId: '\n' +
                    '529718298870-ju4moks4vkha1f2j8n58qqoqfq88c6dq.apps.googleusercontent.com\n',
                clientSecret: '\n' +
                    'Hwb2ylpHd5tkwGfv5bim1XDn\n',
                refreshToken: '1/IcWqSVhgYiN8vwb6-QsUDhyD0BQgsJfE6aHrPayP4w4',
        }}
    );



    let mailOptions = {
        from: 'adzadogo@gmail.com', // sender address
        to: email.email, // receiver email
        subject: "Hello subscription alert", // Subject line
        html: '<p>You are invited to subscribe to this theme please follow the link  <a href="https://itt-new-store.azurewebsites.net/store-details/' + themeId + '"><span style="color:#f46242 ">subscribe to theme</span></a></p>' // html body
    };

    try {
        const info = await transporter.sendMail(mailOptions);
        console.log("Message sent: %s", info.messageId);
        console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
    } catch (e) {
        console.log(e)
    }


}

function getUserEmail(userId) {
    return User.findOne({_id: userId}, {email: 1});
}
