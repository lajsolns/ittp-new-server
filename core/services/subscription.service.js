const mongoose = require('mongoose');
const asyncHandler = require('express-async-handler');

const Subscription = mongoose.model('Subscription');
const Payment = mongoose.model('Payment');
const User = mongoose.model('User');

const paymentService = require('./payment.service');
const userService = require('./user.service');
const themeService = require('./theme.service');
const mediaService = require('./media.service');
const statsService = require('./stats.service');
const wishlistService = require('./wishlist.service');
const accesslistService = require('./accesslist.service');
const azureService = require('./azure-notification.service');


const getThemeSubscribers = asyncHandler(async (themeId) => {
    const subscription = await Subscription.find({themeId, active: true}, {userId: 1, _id: 0}, {sort: {updatedAt: -1}});
    return subscription.map(x => x.userId);
});


exports.getUserSubscriptions = asyncHandler(async (userId) => {
    const subscription = await Subscription.find({userId, active: true}, {themeId: 1, _id: 0}, {sort: {updatedAt: -1}});
    return subscription.map(x => x.themeId);
});


const findActiveSubscription = ({themeId, userId}) => {
    return Subscription.findOne({userId, themeId, active: true}, {}, {sort: {updatedAt: -1}});
};


const getUserThemeSubscriptionStatus = async ({themeId, userId}) => {
    const subs = await findActiveSubscription({themeId, userId});
    const paymentStatus = await paymentService.isSubscriptionPaymentRequired({themeId, userId});
    const hasAccess = await accesslistService.hasAccess({themeId, userId});

    const status = {info: {}};
    status.isSubscribed = !!subs;
    const {required: isPaymentRequired, paymentId} = paymentStatus;
    status.isPaymentRequired = isPaymentRequired;
    status.hasAccess = hasAccess;
    status.paymentId = paymentId;

    if (status.isSubscribed) {
        status.info.subscribedAt = subs.createdAt;
        Object.assign(status.info, subs.tagPermissions);
    }
    return status;
};
exports.getUserThemeSubscriptionStatus = getUserThemeSubscriptionStatus;


exports.getUserThemeSubscriptionInfo = asyncHandler(async (params) => {
    const {themeId, userId} = params;
    const qry = {themeId: themeId, userId: userId};
    const options = {
        select: {
            paymentId: 1,
            unsubscribedAt: 1,
            createdBy: 1,
            updatedBy: 1
        }
    };

    let subscriptions = await Subscription.find(qry, options);

    subscriptions = subscriptions.map(subscription => {
        const obj = {};
        obj.paymentId = subscription.paymentId;
        obj.unsubscribedAt = subscription.unsubscribedAt;
        obj.createdBy = subscription._storage.createdBy;
        obj.updatedBy = subscription._storage.updatedBy;
        return obj;
    });
    return subscriptions;
});


exports.subscribeUserToThemeWishlist = async ({userId, themeId}) => {
    const isOnList = await wishlistService.isThemeOnUserWishlist({userId, themeId})
    if (!isOnList) {
        return 'User not on Wishlist';
    }
    return await subscribeUserToTheme({userId, themeId, isFromWishList: true});
};


const subscribeUserToTheme = async ({userId, themeId, isFromWishList, paymentId}) => {
    const subscription = new Subscription({userId, themeId, isFromWishList, paymentId});
    subscription.active = true;
    const saveSubscription = await subscription.save();
    statsService.updateSubscriptionStats(themeId);

    wishlistService.removeThemeFromUserWishlist({userId, themeId});
    azureService.notifySubscriptionChange(userId);
    return saveSubscription;
};


exports.subscribeUserToThemeNormal = async ({userId, themeId}) => {
    const hasAccess = await accesslistService.hasAccess({userId, themeId});
    if (hasAccess) {
        const status = await getUserThemeSubscriptionStatus({userId, themeId});
        if (status.isPaymentRequired) {
            return {paymentRequired: true};
        } else {
            const subs = await subscribeUserToTheme({userId, themeId, paymentId: status.paymentId});
            return ({id: subs._id.toString()});
        }
    } else {
        return 'Not on Access List';
    }
};


exports.unsubscribeUserFromTheme = async ({themeId, userId}) => {
    const subscription = await findActiveSubscription({themeId, userId});
    if (!subscription) {
        return 'Subscription not found';
    }

    if (subscription.paymentId) {
        await paymentService.cancelUserSubscriptionPayment(subscription.paymentId);
    }

    subscription.active = false;
    await subscription.save();
    azureService.notifySubscriptionChange(userId);
    statsService.updateSubscriptionStats(themeId);
    return 'user unsubscribed';
};


exports.getThemeUsers = async (params) => {
    const {themeId, page, limit} = params;

    const myCustomLabels = {
        totalDocs: 'count',
        docs: 'data',
    };

    const options = {
        page,
        limit,
        select: {userId: 1},
        customLabels: myCustomLabels,
        sort: {createdAt: -1}
    };

    const qry = {themeId, active: true};
    const subscriptions = await Subscription.paginate(qry, options)
    const userIdArray = subscriptions.data.map(x => x.userId);
    const users = await userService.findUsersInIdArray({list: userIdArray, projection: {email: 1, name: 1}})
    subscriptions.data = users.map(y => {
        return {id: y._id.toString(), name: y.name};
    });
    return subscriptions;
};


exports.searchThemeUsers = async ({searchInput, themeId}) => {
    const searchTerm = params.searchInput;
    const options = {
        select: {userId: 1},
        sort: {createdAt: -1}
    };

    let found = await User.find({"name": {"$regex": searchTerm, "$options": "i"}}, {_id: 1, name: 1});

    const ids = await found.map(it => it._id);

    const qry = {userId: {$in: ids}, themeId: params.themeId};
    const newIds = await Subscription.find(qry, {userId: 1});
    const myIds = newIds.map(myId => myId.userId);
    let r = [];
    let i = 0;
    found.forEach(f => {
        if (myIds.includes(f._id.valueOf().toString())) {
            r[i] = f;
            i++;
        }
    });
    return r;
};

exports.getUserSubscriptionsPaginate = async (params) => {
    const {userId, page, limit} = params;

    const myCustomLabels = {
        totalDocs: 'count',
        docs: 'data',
    };

    const options = {
        page,
        limit,
        select: {themeId: 1},
        customLabels: myCustomLabels,
        sort: {createdAt: -1}
    };

    const qry = {userId, active: true};

    const subscriptions = await Subscription.paginate(qry, options);
    const themeIdArray = subscriptions.data.map(x => x.themeId);
    const themes = await themeService.findThemesInIdArray({list: themeIdArray, projection: {basicInfo: 1}});
    subscriptions.data = themes.map(y => {
        const id = y._id.toString();
        return {
            id,
            name: y.basicInfo.name,
            description: y.basicInfo.description,
            imageUrl: mediaService.getMediaUrls(id).themeIcon
        };
    });
    return subscriptions;
};


exports.getUserSubscriptionDetail = async (params) => {
    const {themeId, userId} = params;
    const subs = await Subscription.findOne({themeId, userId});
    // const userId = subs.userId;
    const paymentId = subs.paymentId;
    const subscription = {id: subs._id.toString()};
    subscription.tagPermissions = subs.tagPermissions;

    const payment = await getSubscriptionPayment(paymentId);
    const user = await getSubscriptionUser(userId);
    return {payment, user, subscription};
};


async function getSubscriptionUser(userId) {
    const user = await User.findById(userId);
    const {_id: id, name, profileImageUrl} = user;
    return {id, name, profileImageUrl};
}

async function getSubscriptionPayment(paymentId) {
    const payment = await Payment.findById(paymentId, {details: 1});
    if (payment) {
        const {price, currency} = payment.details;
        return {id: paymentId, price, currency};
    } else {
        return {};
    }
}


exports.updateTagPermissions = ({themeId, userId, tagPermissions}) => {
    return Subscription.findOneAndUpdate({themeId, userId, active: true}, {tagPermissions});
};

exports.getSubscribersCountByTheme = async (themeId) => {

    const date = new Date();
    let subscribers = {month: '', week: '', day: ''};

    subscribers.day = await Subscription.where({
        themeId: themeId,
        createdAt: {$gt: new Date(date.getFullYear(), date.getMonth(), date.getDay() - 1)}
    }).countDocuments();

    subscribers.week = await Subscription.where({
        themeId: themeId,
        createdAt: {
            $gt: new Date(date.getFullYear(), date.getMonth(), date.getDay() - 8),
            $lt: new Date(date.getFullYear(), date.getMonth(), date.getDay() - 1)
        }
    }).countDocuments();

    subscribers.month = await Subscription.where({
        themeId: themeId,
        createdAt: {$gt: new Date(date.getFullYear(), date.getMonth() - 1, date.getDay() - 1)}
    }).countDocuments();
    return subscribers;

};

