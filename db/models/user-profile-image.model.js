const mongoose = require('mongoose');


const profileImageSchema = new mongoose.Schema({
    userId: {type: String, required: true},
    data: {type: Buffer, required: true},
}, {timestamps: true});

profileImageSchema.index({userId: 1});

module.exports = mongoose.model('UserProfileImage', profileImageSchema);

