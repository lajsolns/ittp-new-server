const mongoose = require('mongoose');

const schema = new mongoose.Schema(
    {
        value: {type: Number, required: true},
        label: {type: String, required: true},
        description: {type: String, required: false},
        order: {type: Number, required: true},
    }
);


mongoose.model('Category', schema);
