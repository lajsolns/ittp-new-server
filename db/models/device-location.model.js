const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const storageDetailsSchema = require('../storage-details.schema');

const locationSchema = new mongoose.Schema({
    deviceId: {type: String, required: true},
    location: {
        type: {type: String, default: "Point"},
        coordinates: [Number]
    },
    altitude: {type: Number, default: 0},
    bearing: {type: Number, default: 0},
    speed: {type: Number, default: 0},
    isMockProvider: {type: Boolean, default: false},
    isOffline: {type: Boolean, default: false},
    provider: {type: String, default: 'GPS'},
    acquired: {type: Number, required: true},
    _storage: storageDetailsSchema
}, {timestamps: true});

locationSchema.plugin(mongoosePaginate);
locationSchema.index({deviceId: 1, location: '2dsphere'});

module.exports = mongoose.model('DeviceLocation', locationSchema, 'locations');
