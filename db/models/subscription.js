const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const storageDetailsSchema = require('../storage-details.schema');

const subscriptionSchema = new mongoose.Schema({
    userId: {type: String},
    themeId: {type: String},
    active: {type: Boolean, default: false},
    isFromWishList: {type: Boolean, default: false},
    paymentId: {type: String},
    tagPermissions: {
        canDelete: {type: Boolean, default: false},
        canEdit: {type: Boolean, default: false},
        canCreate: {type: Boolean, default: false},
    },
    _storage: storageDetailsSchema
}, {timestamps: true});

subscriptionSchema.plugin(mongoosePaginate);

subscriptionSchema.index({userId: 1, themeId: 1, active: 1});


module.exports = mongoose.model('Subscription', subscriptionSchema);
