const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const schema = new mongoose.Schema(
    {
        userId: {type: String, required: true},
        themeId: {type: String, required: true}
    }
);
schema.plugin(mongoosePaginate);
schema.index({userId: 1, themeId: 1}, {unique: true});

mongoose.model('Wishlist', schema);
