const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const storageDetailsSchema = require('../storage-details.schema');


const invitationSchema = new mongoose.Schema({
    themeId: {type: String, required: true},
    userId: {type: String, required: false},
    email: {type: String, required: false},
    _storage: storageDetailsSchema
}, {timestamps: true});

invitationSchema.index({themeId: 1});

module.exports = mongoose.model('Invitation', invitationSchema);