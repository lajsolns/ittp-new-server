const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const schema = new mongoose.Schema({
    email: {
        type: String,
        required: true
    },
    username: String,
    name: String,
    isAdmin: {
        type: Boolean,
        default: false
    },
    profileImageUrl: {
        type: String,
        default: "http://s3.amazonaws.com/nvest/Blank_Club_Website_Avatar_Gray.jpg"
    },
    firebaseId: {
        type: String,
        required: true
    },
}, {timestamps: true});

schema.index({email: 1, isAdmin: 1});

schema.plugin(mongoosePaginate);

mongoose.model('User', schema);






