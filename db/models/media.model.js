const mongoose = require('mongoose');
const storageDetailsSchema = require('../storage-details.schema');


const mediaSchema = new mongoose.Schema({
    themeId: {type: String},
    type: {type: String, required: true},
    data: {type: Buffer, required: true},
    _storage: storageDetailsSchema
}, {timestamps: true});

mediaSchema.index({themeId: 1, type: 1});

module.exports = mongoose.model('Media', mediaSchema);

