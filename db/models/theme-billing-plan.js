const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const storageDetailsSchema = require('../storage-details.schema');
const schema = new mongoose.Schema({
    billingPlanId: {type: String, default: '000-000'},
    price: {type: Number, required: true},
    themeId: {type: String, required: true},
    currency: {type: String},
    selfLink: {type: String},
    _storage: storageDetailsSchema
}, {timestamps: true});

schema.plugin(mongoosePaginate);

schema.index({userId: 1, themeId: 1, active: 1});

mongoose.model('ThemeBillingPlan', schema);