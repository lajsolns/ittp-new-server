const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const storageDetailsSchema = require('../storage-details.schema');

const notificationSchema = new mongoose.Schema({
    content: {type: String, required: true},
    userId: {type: String, required: true},
    _storage: storageDetailsSchema
}, {timestamps: true});

notificationSchema.index({createdAt: -1});
notificationSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Notification', notificationSchema);
