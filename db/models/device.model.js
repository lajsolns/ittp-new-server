const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const deviceSchema = new mongoose.Schema({
    userId: {type: String, required: true},
    hardwareId: {type: String, required: true},
    manufacturer: {type: String, required: true},
    model: {type: String, required: true},
    operatingSystem: {type: String, required: true},
    operatingSystemVersion: {type: String, required: true},
    installDate: {type: String, required: true},
    azureRegistrationId: {type: String, required: true},
    token: {type: String, required: true}
}, {timestamps: true});

deviceSchema.plugin(mongoosePaginate);
deviceSchema.index({userId: 1, hardwareId: 1}, {unique: true});

module.exports = mongoose.model('Device', deviceSchema);