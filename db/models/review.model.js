const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const storageDetailsSchema = require('../storage-details.schema');

const reviewSchema = new mongoose.Schema({
    themeId: {type: String, required: true},
    userId: {type: String, required: true},
    name: {type: String, required: true},
    profileImageUrl: {
        type: String,
        default: "http://s3.amazonaws.com/nvest/Blank_Club_Website_Avatar_Gray.jpg"
    },
    rating: {type: Number, default: 0},
    message: {type: String},
    _storage: storageDetailsSchema
}, {timestamps: true});

reviewSchema.plugin(mongoosePaginate);
reviewSchema.index({themeId: 1});

module.exports = mongoose.model('Review', reviewSchema);

