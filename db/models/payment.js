const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const storageDetailsSchema = require('../storage-details.schema');

const payerInfo = {
    id: {type: String, required: true},
    firstName: {type: String, required: true},
    lastName: {type: String, required: true},
    email: {type: String, required: true},
    paymentMethod: {type: String, required: true},
};

const details = {
    billingPlanId: {type: String, required: true},
    billingAgreementId: {type: String, required: true},
    price: {type: String, required: true},
    currency: {type: String, required: true},
    selfLink: {type: String, required: true}
};

const schema = new mongoose.Schema({
    themeId: {type: String, required: true},
    userId: {type: String, required: true},
    active: {type: Number, default: true},
    canceledAt: {type: Number},
    payerInfo: {type: payerInfo, required: true},
    details: {type: details, required: true},
    _storage: storageDetailsSchema
}, {timestamps: true});

schema.plugin(mongoosePaginate);

schema.index({userId: 1, themeId: 1, active: 1});

mongoose.model('Payment', schema);