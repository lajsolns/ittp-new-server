const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const Tag = mongoose.model('Tag');

const storageDetailsSchema = require('../storage-details.schema');
const tagPropertyTypeValidator = require('../property-type-validators');
const unitConverter = require('../unit-converter');

const basicInfoSchema = {
    name: {type: String, required: true},
    description: {type: String, default: ''},
    color: {type: String, default: '#F47D31'},
    defaultDistance: {type: Number, default: 25},
    defaultDistanceMeters: {type: Number, default: 25},
    defaultDistanceUnit: {type: Number, default: 0},
    defaultTagSilence: {type: Number, default: 0},
    defaultTagSilenceSeconds: {type: Number, default: 0},
    defaultTagSilenceUnit: {type: Number, default: 0},
};

const tagPropertySchema = {
    normalizedName: {type: String, required: true},
    name: {type: String, required: true},
    tagPropertyType: {type: Number, default: 1},
    order: {type: Number, default: 1},
    mandatory: {type: Boolean, default: false},
    showOnMap: {type: Boolean, default: false},
    showOnTip: {type: Boolean, default: false}
};

const now = new Date();
const statSchema = {
    tags: {
        count: {type: Number, default: 0},
        updatedAt: {type: Date, default: now}
    },
    wishList: {
        count: {type: Number, default: 0},
        updatedAt: {type: Date, default: now}
    },
    ratings: {
        data: {type: [Number], default: [0, 0, 0, 0, 0]},
        average: {type: Number, default: 0},
        count: {type: Number, default: 0},
        updatedAt: {type: Date, default: now}
    },
    subscriptions: {
        data: {
            subscribe: {type: [Number], default: [0, 0, 0, 0,]},
            unsubscribe: {type: [Number], default: [0, 0, 0, 0,]},
        },
        total: {
            subscribe: {type: Number, default: 0},
            unsubscribe: {type: Number, default: 0}
        },
        updatedAt: {type: Date, default: now}
    }
};


const settingsSchema = {
    visibleInStore: {type: Boolean, default: false},
    allowFastTags: {type: Boolean, default: false},
    allowMovingTags: {type: Boolean, default: false},
    allowCrowdSourcing: {type: Boolean, default: false},
    region: {type: [Number]},
    // region: {type: [Number], default: [0]},
    price: {type: String, default: '0'},
    currency: {type: String, default: 'USD'},
    keyWords: {type: String, default: ''},
    useAccessList: {type: Boolean, default: false},
    category: {type: [Number]}
    // category: {type: [Number], default: [0]}
};

const themeSchema = new mongoose.Schema({
    basicInfo: basicInfoSchema,
    stats: statSchema,
    settings: settingsSchema,
    tagProperties: [tagPropertySchema],
    _storage: storageDetailsSchema
}, {timestamps: true, usePushEach: true});

themeSchema.methods.newTag = function (lat, lon) {
    const newTag = new Tag();
    newTag.themeId = this._id.toString();
    newTag.location.coordinates = [lon, lat];
    newTag.propertyValues = [];
    this.tagProperties.forEach(tagProp => {
        newTag.propertyValues.push({normalizedName: tagProp.normalizedName.toString(), value: null});
    });
    return newTag;
};

themeSchema.methods.validateTag = function (tag) {
    const errList = [];
    this.tagProperties.forEach(tagProperty => {
        const normalizedName = tagProperty.normalizedName;
        const propertyValue = tag.propertyValues.find(x => x.normalizedName === normalizedName);
        if (propertyValue) {
            if (!tagPropertyTypeValidator(tagProperty.tagPropertyType, propertyValue.value)) {
                errList.push({normalizedName: normalizedName, message: `${tagProperty.name}: invalid value`});
            }
        }
        if (tagProperty.mandatory && (!(propertyValue) || !(propertyValue.value))) {
            errList.push({normalizedName: normalizedName, message: `${tagProperty.name}: invalid value`});
        }
    });
    return errList;
};

themeSchema.methods.saveNotificationDistance = function () {
    this.basicInfo.defaultDistanceMeters = unitConverter.convertDistance(this.basicInfo.defaultDistanceUnit, this.basicInfo.defaultDistance);
};

themeSchema.methods.saveSilenceTime = function () {
    this.basicInfo.defaultTagSilenceSeconds = unitConverter.convertTime(this.basicInfo.defaultTagSilenceUnit, this.basicInfo.defaultTagSilence);
};

function hasValue(value) {
    return (value !== null && value !== undefined && value !== '')
}

themeSchema.methods.validateTagField = function (field) {
    const {normalizedName, value} = field;
    const tagProp = this.tagProperties.find(x => x.normalizedName === normalizedName);
    if (hasValue(value)) {
        if (tagPropertyTypeValidator(tagProp.tagPropertyType, value)) {
            return {success: true};
        } else {
            return {success: false, message: 'invalid value'};
        }
    } else {
        if (tagProp.mandatory) {
            return {success: false, message: 'Value Required'};
        } else {
            return {success: true};
        }
    }
};

themeSchema.methods.isPaidTheme = function () {
    return parseFloat(this.settings.price) > 0;
};

themeSchema.plugin(mongoosePaginate);

// themeSchema.index({'_storage.deleted': 1});
themeSchema.index({'_storage.createdBy': 1});
themeSchema.index({'basicInfo.name': 1});
themeSchema.index({'basicInfo.categories': 1});

mongoose.model('Theme', themeSchema);
