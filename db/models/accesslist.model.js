const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const storageDetailsSchema = require('../storage-details.schema');

const accessListSchema = new mongoose.Schema({
    themeId: {type: String, required: true},
    userId: {type: String, required: true},
    _storage: storageDetailsSchema
}, {timestamps: true});

accessListSchema.plugin(mongoosePaginate);
accessListSchema.index({userId: 1, themeId: 1}, {unique: true});


module.exports = mongoose.model('AccessList', accessListSchema);

