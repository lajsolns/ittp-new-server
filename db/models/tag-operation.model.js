const mongoose = require('mongoose');

const tagOperation = new mongoose.Schema({
    userId: {type: String, required: true},
    jobType: {type: String, required: true},
    themeId: {type: String, required: true},
    fileInfo: {
        name: {type: String, required: true},
        path: {type: String, required: true},
        size: {type: String, required: true}
    },
    status: {type: String, required: true},
    hasErrors: {type: Boolean, default: false},
    summary: {
        processedLines: {type: Number, default: 0},
        savedTagsCount: {type: Number, default: 0},
        errorCount: {type: Number, default: 0},
        duration: {type: Number, default: 0},
        errorMessages: {type: String, default: ''}
    },
}, {timestamps: true});

tagOperation.index({themeId: 1});
tagOperation.index({updatedAt: -1});

module.exports = mongoose.model('TagOperation', tagOperation);
