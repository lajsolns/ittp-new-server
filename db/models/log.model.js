const mongoose = require('mongoose');

const logSchema = new mongoose.Schema({
    message: {type: String}
}, {timestamps: true});

logSchema.index({themeId: 1, type: 1});

module.exports = mongoose.model('Logs', logSchema);

