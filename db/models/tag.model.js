const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const storageDetailsSchema = require('../storage-details.schema');


const tagSchema = new mongoose.Schema({
    themeId: {type: String, required: true},
    location: {type: {type: String, default: "Point"}, coordinates: [Number]},
    active: {type: Boolean, default: true},
    propertyValues: [{
        normalizedName: {type: String, required: true},
        value: {type: String}
    }],
    _storage: storageDetailsSchema
}, {timestamps: true});

tagSchema.plugin(mongoosePaginate);
tagSchema.index({themeId: 1, location: '2dsphere'});

module.exports = mongoose.model('Tag', tagSchema);
