"use strict";
Object.defineProperty(exports, "__esModule", {value: true});

const propertyTypes = [
    {value: 0, label: 'Text', order: 1},
    {value: 1, label: 'Phone Number', order: 2},
    {value: 2, label: 'Email Address', order: 3},
    {value: 3, label: 'Web Address', order: 4},
    {value: 4, label: 'Image Link', order: 5},
    {value: 5, label: 'Video Link', order: 6},
    {value: 6, label: 'Numbers Only', order: 7},
    {value: 7, label: 'Address', order: 8}
];

function isValid(type, value) {
    switch (type) {
        case 0:
        case 7:
            return (typeof value === 'string');
        case 6:
            return (typeof value === 'number');
        case 1:
            return (typeof value === 'string');
        // return /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im.test(value);
        case 2:
            return /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(value);
        case 3:
        case 4:
        case 5:
            return /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g.test(value);
        default:
            throw new Error('invalid line2 supplied');
    }
}


module.exports = isValid;
