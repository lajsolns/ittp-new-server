const distance = [
    {value: 0, label: 'Meters', order: 1},
    {value: 1, label: 'Km', order: 2},
    {value: 2, label: 'Feet', order: 3},
    {value: 3, label: 'Yards', order: 4},
    {value: 4, label: 'Miles', order: 5}
];

exports.convertDistance = function (unit, value) {
    switch (unit) {
        case 0:
            return value;
        case 1:
            return value * 1000;
        case 2:
            return value * 0.3048;
        case 3:
            return value * 0.9144;
        case 4:
            return value * 1609.34;
        default:
            throw new Error('Unsupported Unit');
    }

};

const time = [
    {value: 0, label: 'Minutes', order: 1},
    {value: 1, label: 'Hours', order: 2},
    {value: 2, label: 'Days', order: 3},
    {value: 3, label: 'Weeks', order: 4},
    {value: 4, label: 'Months', order: 5}
];

exports.convertTime = function (unit, value) {
    switch (unit) {
        case 0:
            return value * 60;
        case 1:
            return value * 60 * 60;
        case 2:
            return value * 60 * 60 * 24;
        case 3:
            return value * 60 * 60 * 24 * 7;
        case 4:
            return value * 60 * 60 * 24 * 30;
        default:
            throw new Error('Unsupported Unit');
    }

};
